# Commands Cheat Sheet

| Command      | Alias | Description |
| --- | ----------- | ----------- |
| !attack | !atk |  When a monster spawns, you can attack it using your weapon with this command |
| !take |  |  Grabs the last item that fell on the ground and puts it in your backpack |
| !take `groundSlot` |  |  Grabs item on the ground slot `groundSlot` and puts it in your backpack |
| !sell |  |  Sells the last item in your backpack (only available when the travelling merchant is present) |
| !sell `inventorySlot` |  |  Sells the item in your inventory slot `inventorySlot` (only available when the travelling merchant is present) |
| !drop |  |  Drops the last item in your backpack |
| !drop `inventorySlot` |  |  Drops the last item in your backpack |
| !look |  |  Shows the items on the floor that you can !take |
| !equipWeapon `inventorySlot` | !equipW, !ew |  Equips selected item as weapon |
| !equipArmor `inventorySlot` | !equipAr, !ear |  Equips selected item as armor |
| !equipAccessory `inventorySlot` | !equipAc, !eac |  Equips selected item as accessory |
| !unequipWeapon | !unequipW, !uew |  Unequips weapon |
| !unequipArmor | !unequipAr, !uear |  Unequips armor |
| !unequipAccessory | !unequipAc, !ueac |  Unequips accessory |
| !resurrect | !res |  Heals all your wounds and resurrects you, but takes a long time |
| !butcher |  |  Will butcher the last enemy corpse on the floor, takes 30 seconds |
| !eat `inventorySlot` |  |  Will try to eat the item in the `inventorySlot`, but prevent the action if the item is detected as bad to eat |
| !eat `inventorySlot` force | `!eat f`, `!eatf`, `!eat 0 f`, `!eat0f` |  Will try to eat the item in the `inventorySlot`, but prevent the action if the item is detected as bad to eat |
| !studyMagic | !study |  Allows to learn magic |
| !focus `inventorySlot` |  |  Allows to create a magic spell from any item |
| !cast `spellSlot` `player name` | `!cast`, `!cast 0`, `!cast Freyja Ymir`, `!cast 0 Freyja Ymir` |  Allows to cast a focused magic spell on an enemy or player |
| !move `direction` | `!travel east`, `!move north` |  Allows to move from location to location |
| !mine `minutes` | `!mine 1`, `!mine 720` | Go to the mine for X minutes |
| !stopMining | `!stopMin` | Stop mining |
| !buy plot |  | Buy a plot of land in a city (requires a city in your actual location, and 1440 gold) |
| !buy storage |  | Buy a storage slot a plot of land (requires to be in a plot of land, and 720 gold for the first level) |
| !store `inventorySlot` |  | Store an item of your inventory into the current building/plot storage |
| !retrieve `storageSlot` |  | Retrieves an item from the current building/plot storage |
| !store gold `goldAmount` |  | Store gold into the current building/plot. This gold will be used first for further upgrades |