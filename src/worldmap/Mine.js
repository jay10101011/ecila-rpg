import Building from "./Building.js"
import BuildingType from "./BuildingType";

export default class Mine extends Building {
    /** @type {number} */
    tax = 0.2

    constructor(name) {
        super(BuildingType.MINE, name)
    }
}