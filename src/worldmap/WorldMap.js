export default class WorldMap {
    /** @type {Region[]} */
    regions = []

    /** @type {Location[]} */
    locations = []
}