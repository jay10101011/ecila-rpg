import Building from "./Building.js"
import BuildingType from "./BuildingType";

export default class Bank extends Building {
    /** @type {number} */
    tax = 0.02

    constructor(name) {
        super(BuildingType.MINE, name)
    }
}