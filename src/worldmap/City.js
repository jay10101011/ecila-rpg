import Building from "./Building";
import BuildingType from "./BuildingType";

export default class City extends Building {
    /** @type {Building[]} */
    buildings = []

    /** @type {?Mine} */
    mine

    /** @type {?Bank} */
    bank

    /** @type {string} */
    ownerFaction

    constructor(name) {
        super(BuildingType.CITY_HALL, name)
    }
}