import * as fs from "fs"
import PlayerService from "../player/PlayerService.js"
import PlayerGender from "../player/PlayerGender.js"

const WORLD_MAP_FILE = './save/WorldMap.json'

class WorldMapManager {
    /** @type {?WorldMap} */
    static worldMap

    /**
     * @param {Location} location
     * @return {Region} region
     */
    static getRegion(location) {
        return this.worldMap?.regions?.find(r => r.name.toLowerCase() === location.region.toLowerCase())
    }

    /**
     *
     * @param {number} x
     * @param {number} y
     * @return {Location}
     */
    static getLocationByPosition(x, y) {
        return this.worldMap?.locations?.find?.(l => l.positionX === x && l.positionY === y)
    }

    /**
     *
     * @param {number} x
     * @param {number} y
     * @param {number} z
     * @return {Building}
     */
    static getBuildingByPosition(x, y, z) {
        for (let location of this.worldMap?.locations) {
            for (let building of location.city?.buildings || []) {
                if (building.name === `loc-${x.toString().padStart(2, '0')}-${y.toString().padStart(2, '0')}-${z.toString().padStart(3, '0')}`) {
                    return building
                }
            }
        }
    }

    static getBuildingByKey(key) {
        for (let location of this.worldMap?.locations) {
            for (let building of location.city?.buildings || []) {
                if (building.key === key) {
                    return {building, location}
                }
            }
        }
        return  {building: null, location: null}
    }

    /**
     * @param {string} s
     * @return {Location|null}
     */
    static getLocationByString(s) {
        if (!s) return null
        const match = s.match(/loc-([0-9]*)-([0-9]*)/i)
        if (!match) return null
        const x = parseInt(match[1])
        const y = parseInt(match[2])
        return this.getLocationByPosition(x, y)
    }

    /**
     * @param {string} s
     * @return {Building|null}
     */
    static getBuildingByString(s) {
        if (!s) return null
        const match = s.match(/loc-([0-9]*)-([0-9]*)-([0-9]*)/i)
        if (!match) return null
        const x = parseInt(match[1])
        const y = parseInt(match[2])
        const z = parseInt(match[3])
        return this.getBuildingByPosition(x, y, z)
    }

    static getStringLocation(location) {
        return `loc-${location.positionX.toString().padStart(2, '0')}-${location.positionY.toString().padStart(2, '0')}`
    }

    static getDifficulty(location) {
        if (location.difficulty) {
            return location.difficulty[Math.floor(Math.random() * location.difficulty.length)]
        }
        const region = this.getRegion(location)
        if (region.difficulty) return region.difficulty[Math.floor(Math.random() * region.difficulty.length)]
    }

    /**
     *
     * @param {Location} location
     * @return {*}
     */
    static getEnemiesTypes(location) {
        if (location.enemiesTypes) return location.enemiesTypes
        const region = this.getRegion(location)
        if (region.enemiesTypes) return region.enemiesTypes
    }

    static getItemText(item) {
        if (!item) return `[undefined]`
        return `${item?.name} (${item?.rarity} ${item?.type})`
    }

    /**
     *
     * @param {Enemy} enemy
     * @param {?string} name
     */
    static getEnemyInfo(enemy, name = null) {
        const items = PlayerService.getItems(enemy)

        return `[ ${name || 'Attacker'}: ${enemy.name}; difficulty: ${enemy.difficulty}; weapon: ${items.weapon}; armor: ${items.armor}; accessory: ${items.accessory}; wounds: ${enemy.health.wounds.length > 0 ? enemy.health.wounds.join(', ') : 'None'}; blood loss: ${enemy.health.bloodLoss}; status: ${enemy.health.status} ]`
    }

    /**
     *
     * @param {Player} enemy
     * @param {?string} name
     */
    static getPlayerEnemyInfo(enemy, name = null) {
        const items = PlayerService.getItems(enemy)
        return `[ ${name || 'Attacker'}: ${enemy.name}; gender: ${PlayerGender.fromIndex(enemy.gender) || "unspecified"}; race: ${enemy.race}; weapon: ${items.weapon}; armor: ${items.armor}; accessory: ${items.accessory}; wounds: ${enemy.health.wounds.length > 0 ? enemy.health.wounds.join(', ') : 'None'}; blood loss: ${enemy.health.bloodLoss}; status: ${enemy.health.status} ]`
    }

    static save() {
        if (!fs.existsSync('./save')) {
            fs.mkdirSync('./save')
        }
        fs.writeFileSync(WORLD_MAP_FILE, JSON.stringify(this.worldMap, null, 4))
    }

    static load() {
        try {
            this.worldMap = JSON.parse(fs.readFileSync(WORLD_MAP_FILE).toString())
            console.debug(`Successfully loaded WorldMap!`)
        } catch (e) {
            console.error(`Couldn't load WorldMap file`, e)
        }
    }
}

WorldMapManager.load()

export default WorldMapManager