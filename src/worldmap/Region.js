export default class Region {
    /** @type {string} */
    name

    /** @type {?string[]} */
    difficulty

    /** @type {string[]} */
    enemiesTypes

    constructor(name, difficulty = [], enemiesTypes = []) {
        this.name = name
        this.difficulty = difficulty
        this.enemiesTypes = enemiesTypes
    }
}