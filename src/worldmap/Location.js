import Region from "./Region.js"

export default class Location extends Region {

    /** @type {string} */
    channelId

    /** @type {string} */
    region

    /** @type {Enemy} */
    activeEnemy

    /** @type {?Enemy[]} */
    enemyCorpses = []

    /** @type {?Item[]} */
    itemsOnTheFloor = []

    /** @type {?number} */
    positionX

    /** @type {?number} */
    positionY

    /** @type {?number} */
    respawnTime = 180

    /** @type {?City} */
    city

    constructor(name, region, difficulty = null, enemiesTypes = null) {
        super(name, difficulty, enemiesTypes)
        this.region = region
    }
}