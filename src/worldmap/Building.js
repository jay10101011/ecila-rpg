import {v4} from "uuid";

export default class Building {
    /** @type {string} */
    name

    /** @type {number} */
    type

    /** @type {number} */
    level = 0

    /** @type {?string} */
    owner

    /** @type {string} */
    key = v4()

    /** @type {number} */
    gold = 0

    /** @type {?Item[]} */
    itemsOnTheFloor = []

    /** @type {?Enemy[]} */
    enemyCorpses = []

    /** @type {number} */
    storageSize = 0

    /** @type {?Item[]} */
    storage = []

    constructor(type, name, owner = null) {
        this.type = type
        this.name = name
        this.owner = owner
    }
}