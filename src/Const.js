export default class Const {
    static STARTING_LOCATION = 'loc-00-00'
    static STARTING_RACE = 'human'
    static ROLE_PLAYER = 'Player'
    static STATUS_DEAD = ["dead", "killed", "died", "deceased", "defeated", "destroyed", "disabled", "total destruction", "annihilated", "obliterated", "defeated!", "ashes"]
    static FULL_HEALS = ['healed', 'cured', 'healed (general)', 'cured (general)', 'completely healed', 'completely cured',
        'full heal', 'full health', 'full cure', 'fully restored', 'fully healed', 'fully cured', 'recovered', 'fully recovered',
        'fully restored health', 'restored to full health', 'healed (all types)', 'cured (all wounds)', 'no more injuries', 'removed all injuries',
        'resurrected', 'restored (hit points)']
    static NO_DAMAGE = ["no", "n/a", "no damage", "none", "undefined", "blocked", "spared", "missed", "failed attempt", "failed attempt (unsuccessful)", "0", "thrown", "nothing", "unharmed", "uninjured"]
}