class PlayerGender {
    static MALE = 0
    static FEMALE = 1

    static getIndex(name) {
        const formattedName = name.trim().toLowerCase()
        return formattedName === 'male' ? this.MALE : formattedName === 'female' ? this.FEMALE : null
    }

    static fromIndex(id) {
        return id === 0 ? 'male' : id === 1 ? 'female' : null
    }
}

export default PlayerGender