import PlayerGender from "./PlayerGender.js"
import fs from "fs"
import {MessageEmbed} from "discord.js"
import WorldMapManager from "../worldmap/WorldMapManager.js"
import EnemyService from "../entity/EnemyService.js"
import DiscordService from "../util/DiscordService.js"
import Colors from "../util/Colors.js"

const PLAYERS_FILE = './save/Players.json'

class PlayerService {
    static players = {}


    static async levelUpMagic(channel, player, power = 4) {
        if (Math.random() < 1 / (Math.pow(player.magicalLevel + 1, power))) {
            player.magicalLevel++
            const embed = new MessageEmbed()
                .setColor(Colors.GOLD)
                .setTitle(`${player.name} magical level increased!`)


            embed.setDescription(`${player.firstname} can now hold more focused spells!`)
                .addField("New level", `${player.magicalLevel}`, true)

            await channel.send({embeds: [embed]})
            return true
        }
        return false
    }

    static isPlayer(entity) {
        return !!entity.userId
    }

    /**
     *
     * @param {Enemy} enemy
     * @param {Location} location
     * @return {Promise<{weapon: Item, armor: Item, accessory: Item}>}
     */
    static async lootDeadEnemy(enemy, location) {
        if (!location.itemsOnTheFloor) location.itemsOnTheFloor = []
        return {weapon: enemy.weapon, armor: enemy.armor, accessory: enemy.accessory}
    }

    static async enemyIsDead(enemy, player, karmaGain, message, location) {
        if (PlayerService.isPlayer(enemy)) {
            if (!enemy.karma) enemy.karma = 0
            if (!player.karma) player.karma = 0

            karmaGain = enemy.karma < 0 ? 1 : -1
            player.karma += karmaGain
        }

        const embed2 = new MessageEmbed()
            .setColor(Colors.GOLD)
            .setTitle(`${enemy.name} is Dead!`)
            .setDescription(`${player.name} killed ${PlayerService.isPlayer(enemy) ? '' : 'the '}${enemy.name}!`)

        if (!location.itemsOnTheFloor) location.itemsOnTheFloor = []

        if (!PlayerService.isPlayer(enemy)) {
            const loot = await PlayerService.lootDeadEnemy(enemy, location)
            if (loot.weapon) {
                location.itemsOnTheFloor.push(enemy.weapon)
                embed2.addField(`Looted weapon (ground slot [${(location.itemsOnTheFloor.length - 1)}])`, WorldMapManager.getItemText(loot.weapon), true)
            }
            if (loot.armor) {
                location.itemsOnTheFloor.push(enemy.armor)
                embed2.addField(`Looted armor (ground slot [${(location.itemsOnTheFloor.length - 1)}])`, WorldMapManager.getItemText(loot.armor), true)
            }
            if (loot.accessory) {
                location.itemsOnTheFloor.push(enemy.accessory)
                embed2.addField(`Looted accessory (ground slot [${(location.itemsOnTheFloor.length - 1)}])`, WorldMapManager.getItemText(loot.accessory), true)
            }

            if (Math.random() < 0.25) {
                const itemLoot = await EnemyService.getRandomLootFromEnemy(enemy, location)
                location.itemsOnTheFloor.push(itemLoot)
                embed2.addField(`Looted random item (ground slot [${(location.itemsOnTheFloor.length - 1)}])`, WorldMapManager.getItemText(itemLoot), true)
            }

            if (Math.random() < 0.25) {
                const itemLoot = await EnemyService.getRandomLootFromEnemy(enemy, location)
                location.itemsOnTheFloor.push(itemLoot)
                embed2.addField(`Looted random item (ground slot [${(location.itemsOnTheFloor.length - 1)}])`, WorldMapManager.getItemText(itemLoot), true)
            }

            if (Math.random() < 0.25) {
                const itemLoot = await EnemyService.getRandomLootFromEnemy(enemy, location)
                location.itemsOnTheFloor.push(itemLoot)
                embed2.addField(`Looted random item (ground slot [${(location.itemsOnTheFloor.length - 1)}])`, WorldMapManager.getItemText(itemLoot), true)
            }

            if (location.activeEnemy) {
                if (!location.enemyCorpses) location.enemyCorpses = []
                location.enemyCorpses.push(location.activeEnemy)
                location.activeEnemy = null
            }

            await message.channel.send({
                embeds: [
                    embed2
                ]
            })
        }

        if (PlayerService.isPlayer(enemy)) {
            await message.channel.send({
                embeds: [
                    new MessageEmbed()
                        .setColor('#ff0000')
                        .setTitle(`${player.name} killed ${player === enemy ? player.gender ? 'herself' : 'himself' : enemy.name}!`)
                        .setDescription(`${enemy.name} is now dead!`)
                ]
            })

            const guild = DiscordService.bot.guilds.cache.find(g => g.id.toString() === process.env.DISCORD_SERVER_ID)
            const enemyUser = guild.members.cache.find(m => m.user.id === enemy.userId)
            if (enemyUser) {
                await enemyUser.send({
                    embeds: [
                        new MessageEmbed()
                            .setColor('#ff0000')
                            .setTitle(`${player.name} killed you!`)
                            .setDescription(`${player.firstname} murdered you in **Freedom and Consequences**!`)
                    ]
                })
            }

            if (enemy.karma && enemy.karma < 0 && player.karma && player.karma >= 0) {
                const karmaGain = 1
                enemy.karma += karmaGain

                const title = `${enemy.name} gained ${karmaGain > 0 ? '+' : ''}${karmaGain} karma`

                await message.channel.send({
                    embeds: [
                        new MessageEmbed()
                            .setColor('#ff0000')
                            .setTitle(`${title}.`)
                            .setDescription(`${title} for being killed by ${player.name}!`)
                    ]
                })
            }


        }
    }

    static async levelUpStrength(message, player) {
        if (Math.random() < 1 / (Math.pow(player.physicalLevel + 1, 3))) {
            player.physicalLevel++
            const embed = new MessageEmbed()
                .setColor(Colors.GOLD)
                .setTitle(`${player.name} physical level increased!`)
                .setDescription(`${player.firstname} can now hold more items in ${player.gender ? 'her' : 'his'} inventory!`)
                .addField("New inventory size", `${player.physicalLevel} slot`, true)

            await message.channel.send({embeds: [embed]})
        }
    }

    /**
     * @param {*} userId
     * @return {Player}
     */
    static getPlayer(userId) {
        return this.players[userId]
    }

    /**
     * @param {string} name
     * @return {Player}
     */
    static getPlayerByName(name) {
        return Object.values(this.players).find(p => p.name?.toLowerCase?.().trim() === name?.toLowerCase?.().trim())
    }

    /**
     *
     * @param {Player|Enemy} player
     * @return {{weapon: (string), armor: (string), accessory: (string)}}
     */
    static getItems(player) {
        const weapon = player.weapon ? `${player.weapon.name} (${player.weapon.rarity} ${player.weapon.type})` : 'None'
        const armor = player.armor ? `${player.armor.name} (${player.armor.rarity} ${player.armor.type})` : 'None'
        const accessory = player.accessory ? `${player.accessory.name} (${player.accessory.rarity} ${player.accessory.type})` : 'None'
        return {weapon, armor, accessory}
    }

    /**
     *
     * @param {Player} player
     */
    static getPlayerInfo(player) {
        const items = PlayerService.getItems(player)
        return `[ Player: ${player.firstname} ${player.lastname}; gender: ${PlayerGender.fromIndex(player.gender) || "unspecified"}; race: ${player.race}; weapon: ${items.weapon}; armor: ${items.armor}; accessory: ${items.accessory}; wounds: ${player.health.wounds.length > 0 ? player.health.wounds.join(', ') : 'None'}; blood loss level: ${player.health.bloodLoss}; health status: ${player.health.status} ]`
    }

    static save() {
        if (!fs.existsSync('./save')) {
            fs.mkdirSync('./save')
        }
        fs.writeFileSync(PLAYERS_FILE, JSON.stringify(this.players, null, 4))
    }

    static load() {
        try {
            this.players = JSON.parse(fs.readFileSync(PLAYERS_FILE).toString())
            console.debug(`Successfully loaded Players!`)
        } catch (e) {
            console.error(`Couldn't load Players file`, e)
        }
    }
}

PlayerService.load()

export default PlayerService