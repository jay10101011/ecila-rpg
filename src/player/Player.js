import PlayerHealth from "./PlayerHealth.js"
import LivingThing from "../entity/LivingThing.js"
import Const from "../Const.js"
import Item from "../entity/Item.js"

export default class Player extends LivingThing {
    /** @type {string} */
    userId

    /** @type {string} */
    firstname

    /** @type {string} */
    lastname

    /** @type {?number} */
    gender

    /** @type {string} */
    race = Const.STARTING_RACE

    /** @type {number} */
    gold = 0

    /** @type {number} */
    physicalLevel = 0

    /** @type {Item[]} */
    inventory = []

    /** @type {number} */
    magicalLevel = 0

    /** @type {string[]} */
    spells = []

    /** @type {number} */
    butcheringLevel = 0

    /** @type {PlayerHealth} */
    health = new PlayerHealth()

    /** @type {?Item} */
    weapon = new Item('very common', 'weapon', 'Old Rusty Kitchen Knife')

    /** @type {?Item} */
    armor

    /** @type {?Item} */
    accessory

    /** @type {?string} */
    location = Const.STARTING_LOCATION

    /** @type {?string} */
    currentAction = null

    /** @type {?any} */
    currentActionData = null

    /** @type {?string} */
    currentActionDescription = null

    /** @type {?number} */
    currentActionDuration = null

    /** @type {?number} */
    currentActionEndsAt = null

    /** @type {?number} */
    karma = 0

    /** @type {?string[]} */
    keys = []

    /**
     *
     * @param {string} userId
     * @param {string} firstname
     * @param {string} lastname
     * @param {number} gender
     * @param {?string} race
     */
    constructor(userId, firstname, lastname, gender, race = null) {
        super(`${firstname} ${lastname}`)
        this.userId = userId
        this.firstname = firstname
        this.lastname = lastname
        this.gender = gender
        this.race = race ?? "human"
    }
}