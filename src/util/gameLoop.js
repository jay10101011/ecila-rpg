import commands from "../commands/commands.js"
import WorldMapManager from "../worldmap/WorldMapManager.js"
import SavingService from "./SavingService.js"
import DiscordService from "./DiscordService.js"
import PlayerService from "../player/PlayerService.js"

async function save(message) {
    const correctCommand = message.cleanContent.match(/^!save/i)

    if (correctCommand) {
        try {
            SavingService.save()
            message.react("✅").catch(console.error)
        } catch (e) {
            console.error(e)
            message.react("❌").catch(console.error)
        }

        return true
    }

    return false
}

async function load(message) {
    const correctCommand = message.cleanContent.match(/^!load/i)

    if (correctCommand) {
        try {
            SavingService.load()
            message.react("✅").catch(console.error)
        } catch (e) {
            console.error(e)
            message.react("❌").catch(console.error)
        }

        return true
    }

    return false
}

/**
 * @param {*[]} messages
 */
async function gameLoop(messages) {

    const locations = WorldMapManager.worldMap.locations

    const locationPromises = []
    for (let location of locations) {
        if (!location.channelId) {
            console.log("No channel ID...")
            continue
        }

        let channel = DiscordService.bot.channels.cache.get(location.channelId)

        if (!channel) {
            console.log("No channel")
            channel = await DiscordService.bot.channels.fetch(location.channelId).catch(console.error)
        }

        if (!channel) {
            console.log("No channel even after fetching...")
            continue
        }

        const promises = []

        for (let command of commands) {
            if (command.callbackGameLoopChannel) {
                promises.push(command.callbackGameLoopChannel(location, channel, null))
            }
        }

        if (PlayerService.players && Object.values(PlayerService.players)) {
            for (let player of Object.values(PlayerService.players)) {
                if (channel.name !== player.location) {
                    continue
                }
                for (let command of commands) {
                    if (command.callbackGameLoop) {
                        promises.push(command.callbackGameLoop(player, location, channel, null))
                    }
                }
            }
        }

        for (let building of location.city?.buildings || []) {
            let channel = DiscordService.bot.channels.cache.find(c => c.name === building.name)
            if (!channel) {
                console.log("No channel for building")
                channel = await DiscordService.bot.channels.fetch(location.channelId).catch(console.error)
            }
            if (!channel) {
                console.log("No channel for building even after fetching...")
                continue
            }
            for (let command of commands) {
                if (command.callbackGameLoopChannel) {
                    promises.push(command.callbackGameLoopChannel(location, channel, building))
                }
            }

            if (PlayerService.players && Object.values(PlayerService.players)) {
                for (let player of Object.values(PlayerService.players)) {
                    if (channel.name !== player.location) {
                        continue
                    }
                    for (let command of commands) {
                        if (command.callbackGameLoop) {
                            promises.push(command.callbackGameLoop(player, location, channel, building))
                        }
                    }
                }
            }
        }



        locationPromises.push(promises)
    }

    const promises = [processMessage(messages)].concat(locationPromises.map(locationPromise => executeChannelLoop(locationPromise)))

    await Promise.all(promises)

    setTimeout(() => gameLoop(messages), 100)
}

async function processMessage(messages) {
    if (messages.length > 0) {
        const message = messages.shift()
        for (let command of commands) {
            if (await command.exec(message)) break
        }
        if (await save(message)) return console.debug(`Save event`)
        if (await load(message)) return console.debug(`Load event`)
    }
}


async function executeChannelLoop(promises) {
    for (let promise of promises) {
        await promise
    }
}

const timeouts = {}

async function handleMessages(messages) {
    for (let channelName in messages) {
        if (!timeouts[channelName]) {
            timeouts[channelName] = true
            await gameLoop(messages[channelName])
        }
    }

    setTimeout(() => handleMessages(messages), 1000)
}

export default gameLoop