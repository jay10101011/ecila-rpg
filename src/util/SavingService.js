import WorldMapManager from "../worldmap/WorldMapManager.js"
import PlayerService from "../player/PlayerService.js"

class SavingService {
    /** @type {?number} */
    static lastSave = Date.now()

    /** @type {boolean} */
    static needsSave = false

    static save() {
        SavingService.needsSave = true
    }

    static load() {
        WorldMapManager.load()
        PlayerService.load()
    }

    static saveAll() {
        if (SavingService.needsSave) {
            WorldMapManager.save()
            PlayerService.save()
            SavingService.lastSave = Date.now()
            SavingService.needsSave = false
            console.log("Saved!")
        }

        setTimeout(SavingService.saveAll, 5000)
    }
}

SavingService.saveAll()

export default SavingService