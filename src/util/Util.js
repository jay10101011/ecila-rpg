import fs from "fs";

export default class Util {
    static loadJSONFile(filename, silent = false) {
        let file
        try {
            file = fs.readFileSync(filename)
            return JSON.parse(file)
        } catch (err) {
            if (!silent) {
                console.error("Couldn't load JSON file", err)
            }
        }
    }
}