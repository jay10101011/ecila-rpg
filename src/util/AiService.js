import {config} from "dotenv"
import axios from "axios"

config()

let counter = 0

class AiService {
    static novelAIBearerTokens = []

    static getNovelAIBearerToken = async (access_key) => {
        return new Promise((resolve, reject) => {
            axios.post("https://api.novelai.net/user/login", {key: access_key}, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then(r => {
                    resolve(r?.data?.accessToken)
                })
                .catch(err => {
                    reject(err)
                })
        })
    }

    static async loadNovelAIKeys() {
        const keys = process.env.NOVELAI_API_KEY.split(";")

        for (let key of keys) {
            try {
                const bearerToken = await this.getNovelAIBearerToken(key)
                if (!AiService.novelAIBearerTokens.includes(bearerToken)) {
                    AiService.novelAIBearerTokens.push(bearerToken)
                    console.debug(`New bearer token generated!`)
                }
            } catch (e) {
                console.error(e)
            }
        }
    }

    static async generate(request) {
        const backendURL = "https://api.novelai.net/ai"
        let res
        try {
            res = await axios.post(
                backendURL + "/generate",
                request,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': "Bearer " + AiService.novelAIBearerTokens[counter++ % AiService.novelAIBearerTokens.length]
                    }
                }
            )
        } catch (e) {
            console.error("An error happened with the custom proxy", e)
            res = null
        }

        return res?.data
    }
}

AiService.loadNovelAIKeys().then(r => null)

export default AiService