const Colors = {
    GOLD: '#ffff66',
    BLACK: '#000000',
    RED: '#ff0000',
    GREEN: '#00ff00'
}

export default Colors