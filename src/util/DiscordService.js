import {config} from "dotenv"
import {Client, Intents} from 'discord.js'
import gameLoop from "./gameLoop.js"

config()

class DiscordService {
    /** @type {Client} */
    static bot

    static messages = []

    static async main() {
        DiscordService.bot = new Client({
            intents: [
                Intents.FLAGS.GUILDS,
                Intents.FLAGS.GUILD_MESSAGES,
                Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
                Intents.FLAGS.GUILD_MESSAGE_TYPING,
                Intents.FLAGS.GUILD_MEMBERS,
                Intents.FLAGS.GUILD_BANS,
                Intents.FLAGS.GUILD_VOICE_STATES,
                Intents.FLAGS.DIRECT_MESSAGES,
                Intents.FLAGS.DIRECT_MESSAGE_REACTIONS,
                Intents.FLAGS.DIRECT_MESSAGE_TYPING
            ],
            partials: [
                'CHANNEL', // Required to receive DMs
            ],
            allowedMentions: {
                // set repliedUser value to `false` to turn off the mention by default
                repliedUser: false
            }
        })
        console.debug(`Discord bot created`)

        DiscordService.bot.on('ready', async () => {
            console.info(`Discord bot ready`)

            DiscordService.bot.on('messageCreate', async (message) => {
                if (message.channel?.name?.startsWith(`loc-`) || message.channel?.name === `character-creation`) {
                    DiscordService.messages.push(message)
                    /*if (!DiscordService.messages[message.channel.name]) DiscordService.messages[message.channel.name] = []
                    DiscordService.messages[message.channel.name].push(message)*/
                }
            })

            DiscordService.bot.on('interactionCreate', async () => {

            })

            DiscordService.bot.on('guildMemberAdd', async (member) => {
                const channel = await DiscordService?.bot?.channels.fetch(process.env.DISCORD_NEWCOMERS_CHANNEL).catch(console.error)
                const channelCharacterCreation = await DiscordService?.bot?.channels.fetch(process.env.DISCORD_STARTING_CHANNEL).catch(console.error)

                await channel?.send?.(`To create your character **and see the actual game**, go in ${channelCharacterCreation} and type either \`!createCharacter male\` or \`!createCharacter female\``)
            })


            gameLoop(DiscordService.messages).then()
        })

        DiscordService.bot.on('error', console.error)

        await DiscordService.bot.login(process.env.DISCORD_TOKEN).catch(e => console.error(e))
        console.debug(`Discord bot logged in`)
    }
}

DiscordService.main().then()

export default DiscordService