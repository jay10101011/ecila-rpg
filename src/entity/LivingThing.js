export default class LivingThing {
    /** @type {string} */
    name

    /** @type {boolean} */
    isAlive = true

    constructor(name) {
        this.name = name
    }
}