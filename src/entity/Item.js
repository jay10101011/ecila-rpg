export default class Item {
    /** @type {string} */
    name

    /** @type {string} */
    type

    /** @type {string} */
    rarity

    /** @type {?boolean} */
    forSale = false

    /** @type {?number} */
    playerPrice

    /** @type {?string} */
    b64image

    /** @type {?number} */
    droppedAt

    /**
     * @param {string} rarity
     * @param {string} type
     * @param {string} name
     */
    constructor(rarity, type, name) {
        this.rarity = rarity
        this.type = type
        this.name = name
        this.droppedAt = Date.now()
    }

    toString(){
        return `${this.name} (${this.rarity} ${this.type})`
    }
}