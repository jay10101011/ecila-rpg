export default class Spell {
    /** @type {string} */
    name

    /** @type {string} */
    level

    /** @type {string} */
    description

    /**
     * @param {string} level
     * @param {string} name
     * @param {string} description
     */
    constructor(level, name, description) {
        this.level = level
        this.name = name
        this.description = description
    }
}