import GeneratorService from "../generator/GeneratorService.js"
import BaseGameService from "../generator/BaseGameService.js"
import Item from "./Item.js"

export default class EnemyService{
    /**
     *
     * @param {Enemy} enemy
     * @param {Location} location
     * @return {Promise<Item>}
     */
    static async getRandomLootFromEnemy(enemy, location) {
        const object = await GeneratorService.newWorkflow(
            BaseGameService.getGenerators(),
            BaseGameService.getWorkflows(),
            "loot",
            [
                {name: "name", value: enemy.name},
                {name: "difficulty", value: enemy.difficulty},
                {name: "type"},
                {name: "rarity"},
                {name: "item"},
            ]
        )

        return new Item(object.rarity, object.type, object.item)
    }
}