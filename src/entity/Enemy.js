import LivingThing from "./LivingThing.js"
import PlayerHealth from "../player/PlayerHealth.js"

export default class Enemy extends LivingThing {
    /** @type {string} */
    type

    /** @type {string} */
    difficulty

    /** @type {string} */
    encounterDescription

    /** @type {PlayerHealth} */
    health = new PlayerHealth()

    /** @type {Item} */
    weapon = null

    /** @type {Item} */
    armor = null

    /** @type {Item} */
    accessory = null

    /** @type {?number} */
    deadAt = null

    constructor(name, difficulty, type, encounterDescription) {
        super(name);
        this.difficulty = difficulty
        this.type = type
        this.encounterDescription = encounterDescription
    }
}