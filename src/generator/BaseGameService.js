import StreamZip from "node-stream-zip"
import * as fs from "fs";
import Util from "../util/Util.js";

class BaseGameService {
    static generators = null
    static workflows = null

    static loadDefaultGenerators(){
        const files = fs.readdirSync(`./data/generator/`)

        for (const file of files) {
            const json = Util.loadJSONFile(`./data/generator/${file}`)
            if (file === "workflows.json") {
                this.workflows = json
            } else {
                this.generators[file.replace(/\.generator/gi, '')] = json
            }
        }

        console.debug(`Successfully loaded base game`)
    }

    static loadAllGenerators(appendMode = false) {
        if (!appendMode) {
            this.generators = {}
            this.loadDefaultGenerators()
        }

        let zip = new StreamZip({
            file: `./data/generatorMods/default.zip`,
            storeEntries: true
        })

        zip.on('ready', () => {
            // Take a look at the files
            for (const entry of Object.values(zip.entries())) {
                if (entry.name === "workflows.json") {
                    const newWorkflows = JSON.parse(zip.entryDataSync(entry.name).toString('utf8'))
                    for (let key of Object.keys(newWorkflows)){
                        this.workflows[key] = newWorkflows[key]
                    }
                } else {
                    this.generators[entry.name.replace(/\.generator/gi, '')] =
                        JSON.parse(zip.entryDataSync(entry.name).toString('utf8'))
                }
            }

            console.debug(`Successfully loaded mod ./data/generatorMods/default.zip`)

            // Do not forget to close the file once you're done
            zip.close()
        })

        zip.on('error', () => {
            console.debug(`Couldn't extract mod ./data/generatorMods/default.zip`)
        })
    }

    static getGenerators() {
        return (this.generators)
    }

    static getWorkflows() {
        return (this.workflows)
    }

    static getCurrency(){
        return this.generators["priceEstimation"].placeholders["currency"]
    }

}

BaseGameService.loadAllGenerators()

export default BaseGameService