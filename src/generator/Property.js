export default class Property {
    /** @type {string} */
    name

    /** @type {string} */
    replaceBy

    /** @type {boolean} */
    input
}