export default class Generator {
    /** @type {string} */
    name

    /** @type {?string} */
    description

    /** @type {?string} */
    context

    /** @type {Property[]} */
    properties

    /** @type {?Object} */
    placeholders

    /** @type {?Object} */
    aiParameters

    /** @type {?string} */
    aiModel

    /** @type {Object[]} */
    list

    /** @type {Generator[]} */
    submodules
}