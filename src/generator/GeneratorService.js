import aiService from "../util/AiService.js";
import TokenizerService from "../tokenizer/TokenizerService.js";

function shuffleArrayInPlace(array) {
    let currentIndex = array.length, randomIndex;

    // While there remain elements to shuffle...
    while (currentIndex !== 0) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]];
    }

    return array;
}

function shuffleArray(array) {
    try {
        return shuffleArrayInPlace(JSON.parse(JSON.stringify(array)))
    } catch {
        return array
    }
}

const presetKrake = {
    "prefix": "vanilla",
    "use_string": true,
    "min_length": 1,
    "max_length": 150,
    "repetition_penalty_range": 2048,
    "generate_until_sentence": true,
    "use_cache": false,
    "return_full_text": false,
    "stop_sequences": [[9264]],
    "eos_token_id": 9264,
    "temperature": 1.5,
    "top_k": 22,
    "top_p": 0.85,
    "tail_free_sampling": 0.836,
    "repetition_penalty": 1.046,
    "repetition_penalty_frequency": 0,
    "repetition_penalty_presence": 0,
    "order": [3, 0],
    "bad_words_ids": [
        [
            0
        ],
        [
            209,
            0
        ],
        [
            29,
            93,
            8112,
            48,
            5518,
            9465,
            49651
        ],
        [
            654,
            93,
            8112,
            48,
            5518,
            9465,
            49651
        ]
    ]
}


export default class GeneratorService {

    /**
     *
     * @param {Generator} generator
     * @param submoduleName
     * @return {any}
     */
    static mergeSubmodule(generator, submoduleName = null) {
        const module = JSON.parse(JSON.stringify(generator))

        if (!generator.submodules?.[submoduleName]) return module

        const submodule = generator.submodules[submoduleName]

        if (submodule.context) {
            module.context = submodule.context
        }

        if (submodule.properties) {
            module.properties = submodule.properties
        }

        if (submodule.placeholders) {
            module.placeholders = submodule.placeholders
        }

        if (submodule.list) {
            module.list = submodule.list
        }

        return module
    }

    /**
     *
     * @param {Generator} generator
     * @param args
     * @param submoduleName
     * @return {Promise<{Object}>}
     */
    static async generator(generator, args, submoduleName = null) {
        const module = this.mergeSubmodule(generator, submoduleName)

        const prompt = this.getPrompt(
            module,
            args,
            true
        )

        let result = await this.executePrompt(generator, submoduleName, prompt.completePrompt)

        let counter = 0
        while (!result?.output){
            counter ++
            console.error(`Oops, generator didn't receive a response from the AI backend! Counter: ${counter}\nRetrying...`)

            if (counter >= 5){
                throw new Error(`AI backend failed five times in a row...`)
            }

            result = await this.executePrompt(generator, submoduleName, prompt.completePrompt)
        }

        return {
            object: this.parseResult(module, prompt.placeholderPrompt, result.output),
            prompt: prompt.completePrompt,
            result,
            module,
        }
    }

    static async newWorkflow(generators, workflows, workflowName, input) {
        if (!workflows?.[workflowName]) {
            return console.error(`Workflow ${workflowName} doesn't exist`)
        }

        let persistentObject
        if (Array.isArray(input)) {
            persistentObject = {}
            for (let i of input) {
                persistentObject[i.name] = i.value
            }
        } else {
            persistentObject = JSON.parse(JSON.stringify(input))
        }

        for (let w of workflows[workflowName]) {
            await GeneratorService.workflowGenerator(generators, w, persistentObject)
        }

        return persistentObject
    }

    static async workflowGenerator(generators, generatorName, persistentObject) {
        if (Array.isArray(generatorName)) {
            const promises = generatorName.map(
                cgn => GeneratorService.workflowGenerator(generators, cgn, persistentObject)
            )
            await Promise.all(promises)
        } else {
            const inputPropertyNames = Object.keys(persistentObject)

            const submoduleInputs = generators[generatorName].properties
                .filter(p => p.input)
                .map(p => p.name)

            const submodulesContainsAllInput = submoduleInputs.every(sip => inputPropertyNames.includes(sip))

            if (!submodulesContainsAllInput) {
                // console.error(`Not every input are provided inside the generator ${generatorName}!`)
            }

            let orderedProperties = []
            for (let poPropName of inputPropertyNames) {
                for (let geProp of generators[generatorName].properties) {
                    if (poPropName === geProp.name) {
                        orderedProperties.push({
                            name: poPropName,
                            value: persistentObject[poPropName]
                        })
                    }
                }
            }

            for (let geProp of generators[generatorName].properties) {
                if (!inputPropertyNames.includes(geProp.name)) {
                    orderedProperties.push({
                        name: geProp.name,
                        value: null
                    })
                }
            }

            const {
                object,
                prompt,
                result
            } = await GeneratorService.generator(generators[generatorName], orderedProperties, true)


            for (let o in object) {
                persistentObject[o] = object[o]
            }

        }
    }


    /**
     * Generates a prompt given a generic generator
     * @param generator {name: String, description: String, properties: [{name: String, replaceBy: String}], context: String, list: List} Generic generator in parsed JSON
     * @param args {[{name: String, value: String}]} List of arguments, defines the order of the properties too (will stop at first null value)
     * @param shuffle Boolean Whether or not to shuffle the list (useful when there are too many entries to fit in the prompt)
     */
    static getPrompt(generator, args, shuffle = false) {
        const list = shuffle ? shuffleArray(generator.list) : generator.list
        let prompt = generator.context ? generator.context + "\n***\n" : ""

        // Build placeholder prompt
        let placeholderPrompt = this.#buildPlaceholder(generator, args)

        // Build list prompt
        let elemsPrompt = this.#buildList(generator, args, list, placeholderPrompt, prompt)

        let completePrompt = prompt + elemsPrompt + placeholderPrompt

        if (generator.placeholders) {
            for (let placeholderName in generator.placeholders) {
                completePrompt = completePrompt.replace(new RegExp("\\$\\{" + placeholderName + "}", 'g'), generator.placeholders[placeholderName])
            }
        }

        return {
            prompt,
            elemsPrompt,
            placeholderPrompt,
            completePrompt
        }
    }

    static buildParams(generator, submoduleName = null) {
        const defaultParams = JSON.parse(JSON.stringify(presetKrake))
        // TODO: change eos and stop_sequences

        if (!generator) throw new Error("No generator provided")

        if (!submoduleName && !generator.aiParameters) return defaultParams

        // Applies aiParameters from global generator
        if (generator.aiParameters) {
            for (let aiParameter in generator.aiParameters) {
                defaultParams[aiParameter] = generator.aiParameters[aiParameter]
            }
        }

        // Applies aiParameters from submodule
        if (generator.submodules?.[submoduleName]?.aiParameters) {
            for (let aiParameter in generator.submodules[submoduleName].aiParameters) {
                defaultParams[aiParameter] = generator.submodules[submoduleName].aiParameters[aiParameter]
            }
        }

        return defaultParams
    }

    static buildModel(generator, submodule = null) {
        if (!generator) throw new Error("No generator provided")

        let model = "krake-v2"

        if (!submodule && !generator.aiModel) return model

        if (generator.aiModel) model = generator.aiModel

        if (submodule && generator.submodules?.[submodule] && generator.submodules[submodule].aiModel)
            model = generator.submodules[submodule].aiModel

        return model
    }

    static async executePrompt(generator, moduleName, prompt) {
        const params = this.buildParams(generator, moduleName)
        const model = this.buildModel(generator, moduleName)
        return await aiService.generate({input: prompt, parameters: params, model})
    }

    /**
     *
     * @param generator {name: String, description: String, properties: [{name: String, replaceBy: String}], context: String, list: List} Generic generator in parsed JSON
     * @param placeholderPrompt
     * @param result
     * @return {Object}
     */
    static parseResult(generator, placeholderPrompt, result) {
        const r = placeholderPrompt + result
        const split = r.split('\n')
        const values = {}
        for (let property of generator.properties) {
            const replaceBy = property.replaceBy ? property.replaceBy : property.name
            values[property.name] = split
                .find(l => l.startsWith(replaceBy))
                ?.replace(replaceBy, '')
                ?.replace(/<\|endoftext\|>.*/g, '')
                ?.trim()
        }
        return values
    }

    /**
     *
     * @param generator {name: String, description: String, properties: [{name: String, replaceBy: String}], context: String, list: List} Generic generator in parsed JSON
     * @param args {[{name: String, value: String}]} List of arguments, defines the order of the properties too (will stop at first null value)
     * @return {string}
     */
    static #buildPlaceholder(generator, args) {
        let placeholderPrompt = ""
        for (let arg of args) {
            const property = generator.properties.find(p => p["name"] === arg["name"])
            if (!property) continue

            const replaceBy = property.replaceBy ? property.replaceBy : property["name"]
            if (!arg.value) {
                placeholderPrompt += `${replaceBy}`
                break
            } else {
                placeholderPrompt += `${replaceBy} ${arg.value}\n`
            }
        }
        return placeholderPrompt
    }

    /**
     *
     * @param generator {name: String, description: String, placeholders: [], properties: [{name: String, replaceBy: String}], context: String, list: List} Generic generator in parsed JSON
     * @param args {[{name: String, value: String}]} List of arguments, defines the order of the properties too (will stop at first null value)
     * @param list {[Object]}
     * @param placeholderPrompt {String}
     * @param prompt {String}
     * @return {string}
     */
    static #buildList(generator, args, list, placeholderPrompt, prompt) {
        const tokenLimit = 2048 - 200
        let elemsPrompt = ""
        for (let elem of list) {
            let elemPrompt = ""
            for (let arg of args) {
                const property = generator.properties.find(p => p["name"] === arg["name"])
                if (!property) continue

                let value = elem[property.name]
                const replaceBy = property.replaceBy ? property.replaceBy : property.name
                elemPrompt += `${replaceBy} ${value}\n`
            }
            elemPrompt += `***\n`

            if (TokenizerService.encode(prompt + elemsPrompt + elemPrompt + placeholderPrompt).length < tokenLimit) {
                elemsPrompt += elemPrompt
            }
        }
        return elemsPrompt
    }
}