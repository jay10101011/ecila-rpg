import UserCommand from "./UserCommand.js"
import SavingService from "../util/SavingService.js"
import {MessageEmbed} from "discord.js"
import WorldMapManager from "../worldmap/WorldMapManager.js"


const drop = new UserCommand(
    /^!dr?o?p?/i,
    async (message, player, location, building) => {

        const fullCommand = message.cleanContent.match(/^!dr?o?p? ?([0-9]*)?/i)

        if (fullCommand[1] && isNaN(parseInt(fullCommand[1]))) {
            await message.react("❌").catch(console.error)
            await message.reply(`Bad usage. Use like this: \`!drop 0\``)
            return true
        }

        const slot = fullCommand[1] ? parseInt(fullCommand[1]) : (player.inventory.length - 1)

        if (slot >= player.inventory.length || slot < 0) {
            await message.react("❌").catch(console.error)
            await message.reply(`Cannot drop item in slot [${slot}] (slot empty or not found)`)
            return true
        }

        const item = player.inventory[slot]

        const inv = building ? building : location

        inv.itemsOnTheFloor.push(item)
        player.inventory.splice(slot, 1)
        if (item) {
            item.droppedAt = Date.now()
        }

        const embed = new MessageEmbed()
            .setColor('#665500')
            .setTitle(`${player.name} dropped the item [${WorldMapManager.getItemText(item)}] on the floor!`)
            .setDescription(`${player.firstname} dropped the item [${WorldMapManager.getItemText(item)}] on the ground slot [${inv.itemsOnTheFloor.length - 1}]`)

        await message.channel.send({embeds: [embed]})

        await message.delete().catch(console.error)
    }
)

drop.callbackGameLoopChannel = async function (location, channel, building) {
    const inv = building ? building : location
    const items = inv.itemsOnTheFloor || []
    const corpses = inv.enemyCorpses || []

    if (!inv.itemsOnTheFloor) inv.itemsOnTheFloor = []
    inv.itemsOnTheFloor = items.filter(item => {
        if (!item.droppedAt) {
            item.droppedAt = Date.now()
        }
        return Date.now() - item.droppedAt < 1000 * 60 * 60
    })

    inv.enemyCorpses = corpses.filter(corpse => {
        if (!corpse.deadAt) {
            corpse.deadAt = Date.now()
        }
        return Date.now() - corpse.deadAt < 1000 * 60 * 60
    })
}

export default drop