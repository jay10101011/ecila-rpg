import UserCommand from "./UserCommand.js"
import {MessageEmbed} from "discord.js"
import WorldMapManager from "../worldmap/WorldMapManager.js"
import SavingService from "../util/SavingService.js"
import Colors from "../util/Colors.js"

const retrieve = new UserCommand(
    /^!retr?i?e?v?e?/i,
    async (message, player, location, building) => {

        if (!building?.storage || building?.storage?.length === 0) {
            await message.react(`❌`).catch(console.error)
            await message.reply(`${player.name} tried to retrieve an item in storage, but there is no item stored here.`)
            return true
        }

        if (player.inventory.length >= player.physicalLevel) {
            await message.react('🎒').catch(console.error)
            await message.react(`❌`).catch(console.error)
            await message.reply(`${player.name} tried to retrieve an item in storage, but ${player.gender ? 'she' : 'he'} is too encumbered.`)
            return true
        }

        const fullCommand = message.cleanContent.match(/^!retr?i?e?v?e? ?(gold)? ?([0-9]*)?/i)

        if (fullCommand[2] && isNaN(parseInt(fullCommand[2]))) {
            await message.react("❌").catch(console.error)
            await message.reply(`Bad usage. Use like this: \`!retrieve 0\``)
            return true
        }

        const slot = fullCommand[2] ? parseInt(fullCommand[2]) : (building.storage.length - 1)

        if (slot >= building.storage.length || slot < 0) {
            await message.react("❌").catch(console.error)
            await message.reply(`Cannot retrieve item in slot [${slot}] (slot empty or not found)`)
            return true
        }

        const item = building.storage[slot]

        const timeUnavailableInSeconds = 2
        player.currentAction = "retrieving an item"
        player.currentActionData = item
        player.currentActionData.message = message
        player.currentActionDescription = ``
        player.currentActionDuration = timeUnavailableInSeconds
        player.currentActionEndsAt = Date.now() + (1000 * timeUnavailableInSeconds)

        SavingService.save()
    }
)

retrieve.callbackGameLoop = async function (player, location, channel, building) {
    if (player?.currentAction === "retrieving an item") {
        if (Date.now() >= player.currentActionEndsAt) {

            if (!player.currentActionData || !building) {
                await channel.send({
                    embeds: [
                        new MessageEmbed()
                            .setColor(Colors.GOLD)
                            .setTitle(`${player.name} was going to retrieve an item, but it looks like this item isn't there anymore!`)
                            .setDescription(`The item couldn't be found...`)
                    ]
                })
                player.currentAction = null
                player.currentActionData = null
                player.currentActionEndsAt = null
                player.currentActionDuration = null
                player.currentActionDescription = null
                return true
            }

            if (building.storage === null) {
                building.storage = []
            }

            if (building.storage.indexOf(player.currentActionData) === -1) {
                await channel.send({
                    embeds: [
                        new MessageEmbed()
                            .setColor(Colors.GOLD)
                            .setTitle(`${player.name} was going to take an item, but it looks like this item isn't there anymore!`)
                            .setDescription(`The item couldn't be found...`)
                    ]
                })
                player.currentAction = null
                player.currentActionData = null
                player.currentActionEndsAt = null
                player.currentActionDuration = null
                player.currentActionDescription = null
                return true
            }

            if (player.inventory.length >= player.physicalLevel) {
                await channel.send(`${player.name} tried to retrieve an item on the floor, but ${player.gender ? 'she' : 'he'} is too encumbered!`)
                return true
            }

            await player.currentActionData.message.delete().catch(console.error)
            delete player?.currentActionData?.message
            if (player.currentActionData && player.currentActionData.droppedAt)
                player.currentActionData.droppedAt = null
            player.inventory.push(player.currentActionData)
            building.storage.splice(building.storage.indexOf(player.currentActionData), 1)

            await channel.send({
                embeds: [
                    new MessageEmbed()
                        .setColor('#55ff55')
                        .setTitle(`${player.name} retrieves the item ${player.currentActionData?.name}!`)
                        .setDescription(`${player.firstname} retrieves the item **[${WorldMapManager.getItemText(player.currentActionData)}]** and puts it in its inventory slot [${player.inventory.length - 1}].`)
                ]
            })

            player.currentAction = null
            player.currentActionData = null
            player.currentActionEndsAt = null
            player.currentActionDuration = null
            player.currentActionDescription = null
            SavingService.save()
        }
    }
}


export default retrieve