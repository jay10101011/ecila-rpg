import PlayerService from "../player/PlayerService.js"
import WorldMapManager from "../worldmap/WorldMapManager.js"
import {Message, TextChannel} from "discord.js"
import SavingService from "../util/SavingService.js"

export default class UserCommand {
    /** @type {RegExp} */
    correctCommand

    /** @type {(message: Message, player: Player, location: Location, ?building: Building)=>Promise<boolean>} */
    callback

    /** @type {boolean} */
    actionBypass

    /** @type {boolean} */
    channelBypass

    /** @type {(player: Player, location: Location, channel: TextChannel, ?building: Building)=>Promise<null>} */
    callbackGameLoop

    /** @type {(location: Location, channel: TextChannel, ?building: Building)=>Promise<null>} */
    callbackGameLoopChannel

    /**
     *
     * @param {RegExp} correctCommand
     * @param {Function} callback
     * @param {boolean} channelBypass
     * @param {boolean} actionBypass
     */
    constructor(correctCommand, callback, channelBypass = false, actionBypass = false) {
        this.correctCommand = correctCommand
        this.callback = callback
        this.channelBypass = channelBypass
        this.actionBypass = actionBypass
    }

    async exec(message) {
        if (!message.cleanContent.match(this.correctCommand)) return false

        const player = PlayerService.getPlayer(message.author.id)
        const building = WorldMapManager.getBuildingByString(player?.location)
        const location = WorldMapManager.getLocationByString(player?.location)

        const correctChannel = message.channel?.name?.toLowerCase() === player?.location

        if (!correctChannel && !this.channelBypass) {
            await message.react(`❌`).catch(console.error)
            await message.react(`🗺`).catch(console.error)
            return true
        }

        if (!player && !this.channelBypass) {
            await message.react("🚫").catch(console.error)
            await message.reply(`You don't seem to have a character right now, try and create one in #character-creation!`)
            return true
        }

        if (player?.currentAction && !this.actionBypass) {
            await message.react(`❌`).catch(console.error)
            await message.reply(`${player.name} tried to do something, but ${player.gender ? 'she' : 'he'} is currently ${player.currentAction}. (${Math.max(0, Math.floor((player.currentActionEndsAt - Date.now()) / 1000) + 1)}sec left)`).catch(console.error)
            return true
        }

        if (player && !player.isAlive && !this.actionBypass) {
            await message.reply(`${player.name} tried to do something, but ${player.firstname} is dead!`)
            return true
        }

        await this.callback(message, player, location, building)
        SavingService.save()

        return true
    }
}