import UserCommand from "./UserCommand.js"
import GeneratorService from "../generator/GeneratorService.js"
import BaseGameService from "../generator/BaseGameService.js"
import {MessageEmbed} from "discord.js"
import SavingService from "../util/SavingService.js"
import Const from "../Const.js"
import PlayerService from "../player/PlayerService.js"
import WorldMapManager from "../worldmap/WorldMapManager.js"
import Colors from "../util/Colors.js"


/**
 *
 * @param {Player} player
 * @param {Enemy|Player} enemy
 * @return {Promise<void|{player: string, enemy: string, enemyCurrentWounds: string, enemyCurrentBloodLoss: string, enemyCurrentStatus: string, description: string, wounds: string, bloodLoss: string, status: string}>}
 */
async function playerAttackEnemy(player, enemy) {
    const enemyWounds = enemy.health.wounds.length === 0 ? 'None' : [...new Set(enemy.health.wounds)].join(', ')

    const object = await GeneratorService.newWorkflow(
        BaseGameService.getGenerators(),
        BaseGameService.getWorkflows(),
        "attackEnemy",
        [
            {name: "player", value: PlayerService.getPlayerInfo(player)},
            {
                name: "enemy",
                value: PlayerService.isPlayer(enemy) ? WorldMapManager.getPlayerEnemyInfo(enemy, "Target") : WorldMapManager.getEnemyInfo(enemy, "Target")
            },
            {name: "enemyCurrentWounds", value: enemyWounds},
            {name: "enemyCurrentBloodLoss", value: enemy.health.bloodLoss},
            {name: "enemyCurrentStatus", value: enemy.health.status}
        ]
    )

    if (object.wounds && object.wounds.trim() && !Const.NO_DAMAGE.includes(object.wounds.trim().toLowerCase())) {
        const parsedWounds = object.wounds.toLowerCase()
            .split(/[;,+]./)
            .map(e => e.trim())
        const newWounds = [...new Set(parsedWounds)]

        enemy.health.wounds?.push?.(...newWounds)
        enemy.health.wounds = [...new Set(enemy.health.wounds)].filter(w => !['none', 'healed', 'cured'].some(e => w.toLowerCase().includes(e)))
    }

    if (object.bloodLoss && object.bloodLoss.trim()) {
        enemy.health.bloodLoss = object.bloodLoss.trim()
    }

    if (object.status && object.status.trim() && !["failed"].includes(object.status.trim().toLowerCase())) {
        enemy.health.status = object.status.trim().toLowerCase()
    }

    if (object.status && object.status.trim() && Const.STATUS_DEAD.includes(object.status.trim().toLowerCase())) {
        enemy.isAlive = false

        if (!PlayerService.isPlayer(enemy)) enemy.deadAt = Date.now()
    }

    return object
}

/**
 *
 * @param {Player} player
 * @param {Enemy|Player} enemy
 * @return {Promise<void|{player: string, enemy: string, enemyCurrentWounds: string, enemyCurrentBloodLoss: string, enemyCurrentStatus: string, description: string, wounds: string, bloodLoss: string, status: string}>}
 */
async function enemyAttackPlayer(player, enemy) {
    const playerWounds = player.health.wounds.length === 0 ? 'None' : [...new Set(player.health.wounds)].join(', ')

    const object = await GeneratorService.newWorkflow(
        BaseGameService.getGenerators(),
        BaseGameService.getWorkflows(),
        "enemyAttack",
        [
            {name: "player", value: PlayerService.getPlayerInfo(player)},
            {
                name: "enemy",
                value: PlayerService.isPlayer(enemy) ? WorldMapManager.getPlayerEnemyInfo(enemy, "Attacker") : WorldMapManager.getEnemyInfo(enemy, "Attacker")
            },
            {name: "enemyCurrentWounds", value: playerWounds},
            {name: "enemyCurrentBloodLoss", value: player.health.bloodLoss},
            {name: "enemyCurrentStatus", value: player.health.status},
        ]
    )

    if (object.wounds && object.wounds.trim() && !Const.NO_DAMAGE.includes(object.wounds.trim().toLowerCase())) {
        const parsedWounds = object.wounds.toLowerCase()
            .split(/[;,+]./)
            .map(e => e.trim())
        const newWounds = [...new Set(parsedWounds)]

        player.health.wounds?.push?.(...newWounds)
        player.health.wounds = [...new Set(player.health.wounds)].filter(w => !['none', 'healed', 'cured'].some(e => w.toLowerCase().includes(e)))
    }

    if (object.bloodLoss && object.bloodLoss.trim()) {
        player.health.bloodLoss = object.bloodLoss.trim()
    }

    if (object.status && object.status.trim() && !["failed"].includes(object.status.trim().toLowerCase())) {
        player.health.status = object.status.trim().toLowerCase()
    }

    if (object.status && object.status.trim() && Const.STATUS_DEAD.includes(object.status.trim().toLowerCase())) {
        player.isAlive = false
    }

    return object
}


async function enemyIsAlive(player, enemy, message) {
    const objectCounterAttack = await enemyAttackPlayer(player, enemy)

    const woundsTextCounterAttack = `\n\nAll player wounds: ${[...new Set(player.health.wounds)].join(', ') || 'none'}`
    const debugText = message.cleanContent.match(/^!att?a?c?k?(d?)/i)[1] ? `\n\n${objectCounterAttack.player}\n\n${objectCounterAttack.enemy}` : ``


    let title = `${PlayerService.isPlayer(enemy) ? '' : 'The '}${enemy.name} attacks ${player.name}!`

    const embed2 = new MessageEmbed()
        .setColor('#ff0000')
        .setTitle(title)
        .setDescription(`${objectCounterAttack.description || '[undefined]'}${woundsTextCounterAttack}${debugText}`)
        .addField(`New wound(s)`, objectCounterAttack.wounds || '[undefined]', true)
        .addField('New blood loss', objectCounterAttack.bloodLoss || '[undefined]', true)
        .addField('New status', objectCounterAttack.status || '[undefined]', true)
        .addField('Is dead?', Const.STATUS_DEAD.includes(objectCounterAttack.status.trim().toLowerCase()).toString(), true)

    if (PlayerService.isPlayer(enemy)) {
        await PlayerService.levelUpStrength(message, enemy)
    }
    await message.channel.send({embeds: [embed2]})

    if (!player.isAlive) {
        await message.channel.send({
            embeds: [
                new MessageEmbed()
                    .setColor(Colors.BLACK)
                    .setTitle(`${player.name} is Dead!`)
                    .setDescription(`${PlayerService.isPlayer(enemy) ? '' : 'The '}${enemy.name} killed ${player.name}!`)
            ]
        })
    }
}

const attack = new UserCommand(
    /^!att?a?c?k?d?/i,
    async (message, player, location, building) => {

        const fullCommand = message.cleanContent.match(/^!att?a?c?k?d? ?([^\n]*)/i)

        let enemy
        let karmaGain = 0
        if (fullCommand[1]) {
            const characterName = fullCommand[1].trim()
            enemy = PlayerService.getPlayerByName(characterName)
            if (!enemy) {
                await message.react("❌").catch(console.error)
                await message?.reply?.(`${player.name} tried to attack ${characterName.name}, but the target couldn't be found!`)
                return true
            }

            if (enemy.location !== player.location) {
                await message.react("❌").catch(console.error)
                await message?.reply?.(`${player.name} tried to attack ${characterName.name}, but the target couldn't be found in this location!`)
                return true
            }

            if (!enemy.karma) enemy.karma = 0
            if (!player.karma) player.karma = 0

            karmaGain = enemy.karma < 0 ? 0 : -1
            player.karma += karmaGain

        } else {
            if (building){
                await message.react("❌").catch(console.error)
                await message.reply(`${player.name} tried to attack an enemy, but ${player.firstname} is currently not outside.`)
                return true
            }

            enemy = location?.activeEnemy
        }


        if (!enemy) {
            await message.react("❌").catch(console.error)
            await message?.reply?.(`${player.name} tried to attack, but there is currently no enemy!`)
            return true
        }

        if (!enemy.isAlive) {
            await message.react("❌").catch(console.error)
            await message?.reply?.(`${player.name} tried to attack ${enemy.name}, but the target is already dead!`)
            return true
        }

        await message.react("⚔️").catch(console.error)

        const timeUnavailableInSeconds = 20
        player.currentAction = "tired"
        player.currentActionData = null
        player.currentActionDescription = null
        player.currentActionDuration = timeUnavailableInSeconds
        player.currentActionEndsAt = Date.now() + (1000 * timeUnavailableInSeconds)

        const object = await playerAttackEnemy(player, enemy)
        const debugText = message?.cleanContent?.match(/^!att?a?c?k?(d?)/i)[1] ? `\n\n${object.player}\n\n${object.enemy}` : ``

        const woundsText = `\n\nAll enemy wounds: ${[...new Set(enemy.health.wounds)].join(', ') || 'none'}`

        const embed = new MessageEmbed()
            .setColor('#ff2222')
            .setTitle(`${player.name} attacks the ${enemy.name}!`)
            .setDescription(`${(object.description || '[undefined]') + (karmaGain ? ` (karma ${karmaGain > 0 ? '+' : ''}${karmaGain} for attacking a good guy)` : ``)}${woundsText}${debugText}`)
            .addField(`New wound(s)`, object.wounds || '[undefined]', true)
            .addField('New blood loss', object.bloodLoss || '[undefined]', true)
            .addField('New status', object.status || '[undefined]', true)
            .addField('Is dead?', Const.STATUS_DEAD.includes(object.status.trim().toLowerCase()).toString(), true)
        // .addField('All wounds', [...new Set(enemy.health.wounds)].join('\n') || 'none', false)
        await message.channel.send({embeds: [embed]})

        await PlayerService.levelUpStrength(message, player)

        if (!enemy.isAlive) {
            await PlayerService.enemyIsDead(enemy, player, karmaGain, message, location)
        } else {
            await enemyIsAlive(player, enemy, message)
        }

        await message.delete().catch(console.error)
        SavingService.save()
    }
)

attack.callbackGameLoop = async function (player, location, channel) {
    if (player?.currentAction === "tired") {
        const enemyDead = !(location.activeEnemy?.isAlive)
        if (enemyDead || Date.now() >= player.currentActionEndsAt) {

            player.currentAction = null
            player.currentActionData = null
            player.currentActionEndsAt = null
            player.currentActionDuration = null
            player.currentActionDescription = null
            SavingService.save()
        }
    }
}

export default attack