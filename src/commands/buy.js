import UserCommand from "./UserCommand.js"
import {MessageEmbed, Permissions} from "discord.js"
import Building from "../worldmap/Building.js"
import BuildingType from "../worldmap/BuildingType.js"
import DiscordService from "../util/DiscordService.js"
import Colors from "../util/Colors.js"

const Prices = {
    PLOT: 1440,
    STORAGE: 720
}

async function buyPlot(message, player, location, building) {
    if (player.gold < Prices.PLOT) {
        await message.react("❌").catch(console.error)
        await message.reply(`${player.name} tried to buy a plot of land, but doesn't have enough gold. (${player.gold}/${Prices.PLOT} gold)`)
        return true
    }

    const plotNamePos = `loc-${location.positionX.toString().padStart(2, '0')}-${location.positionY.toString().padStart(2, '0')}-${location.city.buildings.length.toString().padStart(3, '0')}`

    const plot = new Building(BuildingType.HOUSE, plotNamePos, player.userId)

    const guild = DiscordService.bot.guilds.cache.find(g => g.id.toString() === process.env.DISCORD_SERVER_ID)

    const newRole = await guild.roles
        .create({
            name: `${plotNamePos}`,
            data: {
                name: `${plotNamePos}`,
                color: 'GREEN',
            },
            reason: 'location',
        })
        .catch(console.error)

    const newChannel = await guild.channels.create(plotNamePos, {
        type: 'GUILD_TEXT',
        parent: guild.channels.cache.find(c => c.name.toLowerCase() === `freedom & consequences`)?.id,
        topic: `${player.name}'s plot of land`,
        permissionOverwrites: [
            {
                id: guild.roles.everyone.id,
                deny: [Permissions.FLAGS.VIEW_CHANNEL],
            },
        ],
    }).catch(console.error)

    await newChannel.permissionOverwrites.create(newRole.id, {VIEW_CHANNEL: true}).catch(console.error)

    location.city.buildings.push(plot)

    if (!player.keys) player.keys = []
    player.keys.push(plot.key)
    player.gold -= Prices.PLOT

    await message.channel.send({
        embeds: [new MessageEmbed()
            .setColor(Colors.GOLD)
            .setTitle(`${player.name} bought the plot of land!`)
            .setDescription(`${player.firstname} is now the owner of the plot [${location.city.buildings.length - 1}] in the city ${location.city.name}!\n${player.gender ? 'She' : 'He'} can now use \`!enter X\` to go inside!`)]
    })

    await message.delete().catch(console.error)
}

const buy = new UserCommand(
    /^!buy/i,
    async (message, player, location, building) => {

        const fullCommand = message.cleanContent.match(/^!buy ?(plot|storage)? ?([0-9]*)?/i)

        if (!fullCommand[1]) {
            await message.react("❌").catch(console.error)
            await message.reply(`Bad usage. Use like this: \`!buy plot\``)
            return true
        }

        if (!location.city) {
            await message.react("❌").catch(console.error)
            await message.reply(`${player.name} tried to buy a plot of land, but there is no city in this location!`)
            return true
        }

        if (fullCommand[1].toLowerCase() === 'plot') {
            await buyPlot(message, player, location, building)
        } else if (fullCommand[1].toLowerCase() === 'storage') {
            if (!building) {
                await message.react("❌").catch(console.error)
                await message.reply(`${player.name} tried to buy storage for a plot of land, but ${player.firstname} is currently not in a land plot!`)
                return true
            }

            const price = Math.ceil(Prices.STORAGE + (Prices.STORAGE * (building.storageSize || 0) * 0.2))
            if (building.gold === null) building.gold = 0
            if ((player.gold + building.gold) < price) {
                await message.react("❌").catch(console.error)
                await message.reply(`${player.name} tried to buy plot storage, but doesn't have enough gold. (${player.gold + building.gold}/${price} gold)`)
                return true
            }

            if (!building.storage) building.storage = []
            if (!building.storageSize) building.storageSize = 0
            building.storageSize = building.storageSize + 1

            const buildingPaidGold = Math.min(building.gold, price)
            building.gold -= buildingPaidGold
            const playerPaidGold = (price - buildingPaidGold)
            player.gold -= playerPaidGold

            await message.channel.send({
                embeds: [new MessageEmbed()
                    .setColor(Colors.GOLD)
                    .setTitle(`${player.name} bought more storage!`)
                    .setDescription(`${player.firstname} paid ${playerPaidGold} gold to upgrade the storage of this plot. (${buildingPaidGold} paid with the plot gold reserve)`)
                    .addField(`New storage`, `${building.storageSize}`)
                ]
            })

            await message.delete().catch(console.error)
        }
    }
)

export default buy