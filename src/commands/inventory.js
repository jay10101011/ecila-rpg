import UserCommand from "./UserCommand.js"
import {MessageEmbed} from "discord.js"
import PlayerGender from "../player/PlayerGender.js"
import WorldMapManager from "../worldmap/WorldMapManager.js"


function getKeys(keys = []) {
    return keys.map((k, i) => {
        const {building, location} = WorldMapManager.getBuildingByKey(k)
        if (!building) return `${i}: [REVOKED]`
        return `${i}: ${building.name}`
    }).join(', ') || 'None'
}

const inventory = new UserCommand(
    /^!inve?n?t?o?r?y?/i,
    async (message, player, location, building) => {

        const itemList = player.inventory
            .map(
                (item, n) =>
                    `\n${n}: [${item?.rarity} ${item?.type}] "${item?.name}"${item?.b64image ? ' :frame_photo:' : ''}${item?.forSale ? ` (:moneybag: ${item?.playerPrice} gold)` : ''}`
            )
            .join('')

        const embed = new MessageEmbed()
            .setColor('#887733')
            .setTitle(`Character: ${player.name}, ${PlayerGender.fromIndex(player.gender)} ${player.race}`)
            .setDescription(`Inventory (${player.inventory.length}/${player.physicalLevel}):${itemList}`)
            .addField('Current action', `${player.currentAction || 'None'}`, true)
            .addField('Karma', `${player.karma || '0'}`, true)
            .addField('Gold', `${player.gold || '0'}`, true)
            .addField('Keys', `${getKeys(player.keys)}`, false)
            .addField('Physical level', `${player.physicalLevel || '0'}`, true)
            .addField('Magic level', `${player.magicalLevel || 'None'}`, true)
            .addField('Butchering level', `${player.butcheringLevel || '0'}`, true)
            .addField('Equipped weapon', !player.weapon ? 'No Weapon' : `[${player.weapon.rarity} ${player.weapon.type}] ${player.weapon.name}${player.weapon.b64image ? ' :frame_photo:' : ''}`, true)
            .addField('Equipped armor', !player.armor ? 'No Armor' : `[${player.armor.rarity} ${player.armor.type}] ${player.armor.name}${player.armor.b64image ? ' :frame_photo:' : ''}`, true)
            .addField('Equipped accessory', !player.accessory ? 'No accessory' : `[${player.accessory.rarity} ${player.accessory.type}] ${player.accessory.name}${player.accessory.b64image ? ' :frame_photo:' : ''}`, true)
            .addField('Health', player.health.status || 'healthy', true)
            .addField('Blood loss', player.health.bloodLoss || 'none', true)
            .addField('Wounds', player.health.wounds.join(', ') || 'none', true)
            .addField('Focused spells', player.spells.join(', ') || 'none', true)

        await message.channel.send({embeds: [embed]})

        await message.delete().catch(console.error)
    }
)

inventory.actionBypass = true

export default inventory