import UserCommand from "./UserCommand.js"
import {MessageEmbed} from "discord.js"
import WorldMapManager from "../worldmap/WorldMapManager.js"
import SavingService from "../util/SavingService.js"
import Colors from "../util/Colors.js"

const take = new UserCommand(
    /^!ta?k?e?/i,
    async (message, player, location, building) => {
        const inv = building ? building : location

        if (!inv?.itemsOnTheFloor || inv?.itemsOnTheFloor?.length === 0) {
            await message.react(`❌`).catch(console.error)
            await message.reply(`${player.name} tried to take an item on the floor, but there is no item here.`)
            return true
        }


        if (player.inventory.length >= player.physicalLevel) {
            await message.react('🎒').catch(console.error)
            await message.react(`❌`).catch(console.error)
            await message.reply(`${player.name} tried to take an item on the floor, but ${player.gender ? 'she' : 'he'} is too encumbered.`)
            return true
        }

        const fullCommand = message.cleanContent.match(/^!ta?k?e? ?([0-9]*)?/i)


        if (fullCommand[1] && isNaN(parseInt(fullCommand[1]))) {
            await message.react("❌").catch(console.error)
            await message.reply(`Bad usage. Use like this: \`!take 0\``)
            return true
        }

        const slot = fullCommand[1] ? parseInt(fullCommand[1]) : (inv.itemsOnTheFloor.length - 1)

        if (slot >= inv.itemsOnTheFloor.length || slot < 0) {
            await message.react("❌").catch(console.error)
            await message.reply(`Cannot take item in slot [${slot}] (slot empty or not found)`)
            return true
        }

        const item = inv.itemsOnTheFloor[slot]

        const timeUnavailableInSeconds = 2
        player.currentAction = "taking an item"
        player.currentActionData = item
        player.currentActionData.message = message
        player.currentActionDescription = ``
        player.currentActionDuration = timeUnavailableInSeconds
        player.currentActionEndsAt = Date.now() + (1000 * timeUnavailableInSeconds)

        SavingService.save()
    }
)

take.callbackGameLoop = async function (player, location, channel, building) {
    if (player?.currentAction === "taking an item") {
        if (Date.now() >= player.currentActionEndsAt) {

            if (!player.currentActionData) {
                await channel.send({
                    embeds: [
                        new MessageEmbed()
                            .setColor(Colors.GOLD)
                            .setTitle(`${player.name} was going to sell an item, but it looks like this item isn't there anymore!`)
                            .setDescription(`The item couldn't be found...`)
                    ]
                })
                player.currentAction = null
                player.currentActionData = null
                player.currentActionEndsAt = null
                player.currentActionDuration = null
                player.currentActionDescription = null
                return true
            }
            const inv = building ? building : location

            if (inv.itemsOnTheFloor.indexOf(player.currentActionData) === -1) {
                await channel.send({
                    embeds: [
                        new MessageEmbed()
                            .setColor(Colors.GOLD)
                            .setTitle(`${player.name} was going to take an item, but it looks like this item isn't there anymore!`)
                            .setDescription(`The item couldn't be found...`)
                    ]
                })
                player.currentAction = null
                player.currentActionData = null
                player.currentActionEndsAt = null
                player.currentActionDuration = null
                player.currentActionDescription = null
                return true
            }

            if (player.inventory.length >= player.physicalLevel) {
                await channel.send(`${player.name} tried to take an item on the floor, but ${player.gender ? 'she' : 'he'} is too encumbered!`)
                return true
            }

            await player.currentActionData.message.delete().catch(console.error)
            delete player?.currentActionData?.message
            if (player.currentActionData && player.currentActionData.droppedAt)
                player.currentActionData.droppedAt = null
            player.inventory.push(player.currentActionData)
            inv.itemsOnTheFloor.splice(inv.itemsOnTheFloor.indexOf(player.currentActionData), 1)

            await channel.send({
                embeds: [
                    new MessageEmbed()
                        .setColor('#55ff55')
                        .setTitle(`${player.name} takes the item ${player.currentActionData?.name}!`)
                        .setDescription(`${player.firstname} grabs the item **[${WorldMapManager.getItemText(player.currentActionData)}]** and puts it in its inventory slot [${player.inventory.length - 1}].`)
                ]
            })

            player.currentAction = null
            player.currentActionData = null
            player.currentActionEndsAt = null
            player.currentActionDuration = null
            player.currentActionDescription = null
            SavingService.save()
        }
    }
}


export default take