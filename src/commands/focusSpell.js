import UserCommand from "./UserCommand.js"
import SavingService from "../util/SavingService.js"
import {MessageEmbed} from "discord.js"
import WorldMapManager from "../worldmap/WorldMapManager.js"
import GeneratorService from "../generator/GeneratorService.js"
import BaseGameService from "../generator/BaseGameService.js"
import PlayerService from "../player/PlayerService.js"


const focusSpell = new UserCommand(
    /^!focusS?p?e?l?l?/i,
    async (message, player, location, building) => {

        if (player.spells.length >= player.magicalLevel) {
            await message.react('🎒').catch(console.error)
            await message.react(`❌`).catch(console.error)
            await message.reply(`${player.name} tried to focus an item into a spell, but ${player.gender ? 'her' : 'his'} magical level is too low to memorize more spells.`)
            return true
        }

        const fullCommand = message.cleanContent.match(/^!focusS?p?e?l?l? ?([0-9]*)?/i)

        if (fullCommand[1] && isNaN(parseInt(fullCommand[1]))) {
            await message.react("❌").catch(console.error)
            await message.reply(`Bad usage. Use like this: \`!focusSpell 0\``)
            return true
        }

        const slot = fullCommand[1] ? parseInt(fullCommand[1]) : (player.inventory.length - 1)

        if (slot >= player.inventory.length || slot < 0) {
            await message.react("❌").catch(console.error)
            await message.reply(`Cannot use item in slot [${slot}] (slot empty or not found)`)
            return true
        }

        const item = player.inventory[slot]

        if (!item) {
            await message.reply(`Oops, something went wrong, this is a bug!`)
            return true
        }

        const timeUnavailableInSeconds = (60 - ((player.magicalLevel - 1) * 5)) * (building ? 1 : 5)
        player.currentAction = "focusing a spell"
        player.currentActionData = item
        player.currentActionData.message = message
        player.currentActionDescription = ``
        player.currentActionDuration = timeUnavailableInSeconds
        player.currentActionEndsAt = Date.now() + (1000 * timeUnavailableInSeconds)

        await message.channel.send({
            embeds: [
                new MessageEmbed()
                    .setColor('#ffffff')
                    .setTitle(`${player.name} begins to focus a magic spell!`)
                    .setDescription(`${player.firstname} is now focusing a magic spell from the item [${WorldMapManager.getItemText(item)}]. (${timeUnavailableInSeconds}sec left)`)
            ]
        })

        await message.delete().catch(console.error)

        SavingService.save()
    }
)

async function getSpellFromItem(item) {
    const object = await GeneratorService.newWorkflow(
        BaseGameService.getGenerators(),
        BaseGameService.getWorkflows(),
        "focusSpell",
        [
            {name: "item", value: WorldMapManager.getItemText(item)},
            {name: "spell"},
        ]
    )

    return object?.spell
}

focusSpell.callbackGameLoop = async function (player, location, channel) {
    if (player?.currentAction === "focusing a spell" && Date.now() >= player.currentActionEndsAt) {

        const spell = await getSpellFromItem(player.currentActionData)

        await channel.send({
            embeds: [
                new MessageEmbed()
                    .setColor('#ffffff')
                    .setTitle(`${player.name} finished focusing a spell!`)
                    .setDescription(`${player.firstname} produced the spell **${spell}** from the item [${WorldMapManager.getItemText(player.currentActionData)}]`)
            ]
        })

        await PlayerService.levelUpMagic(channel, player)

        player.spells.push(spell)

        player.currentAction = null
        player.currentActionData = null
        player.currentActionEndsAt = null
        player.currentActionDuration = null
        player.currentActionDescription = null
        SavingService.save()
    }
}

focusSpell.callbackGameLoopChannel = async function (location, channel) {

}

export default focusSpell