import {config} from "dotenv"
import UserCommand from "./UserCommand.js"
import {MessageEmbed} from "discord.js"
import DiscordService from "../util/DiscordService.js"
import WorldMapManager from "../worldmap/WorldMapManager.js"
import SavingService from "../util/SavingService.js"
import Colors from "../util/Colors.js"

config()

const POSSIBLE_DIRECTIONS = [
    'east',
    'west',
    'north',
    'south',
    'e',
    'w',
    's',
    'n',
    '',
]

const travel = new UserCommand(
    /^!(travel|move)/i,
    async (message, player, location) => {

        const fullCommand = message.cleanContent.match(/^!(travel|move) ?([^\n]*)/i)

        if (!fullCommand[2]) {
            await message.react("❌").catch(console.error)
            await message.reply(`Bad usage. Use like this: \`!travel east\``)
            return true
        }

        const direction = fullCommand[2].trim().toLowerCase()

        let dirX = 0
        let dirY = 0

        if (direction === 'ne' || direction.startsWith('north e') || direction === 'se' || direction.startsWith('south e') || direction.startsWith('e')) dirX = 1
        if (direction === 'nw' || direction.startsWith('north w') || direction === 'sw' || direction.startsWith('north w') || direction.startsWith('w')) dirX = -1
        if (direction === 'ne' || direction.startsWith('n') || direction === 'nw') dirY = -1
        if (direction === 'se' || direction.startsWith('s') || direction === 'sw') dirY = 1

        const posX = location.positionX + dirX
        const posY = location.positionY + dirY

        const travelLocation = WorldMapManager.getLocationByPosition(posX, posY)

        if (!travelLocation) {
            await message.react("❌").catch(console.error)
            await message.reply(`It looks like there is nothing ${direction.toLowerCase()}! (X: ${posX}; Y: ${posY}; location: ${travelLocation}; direction: ${direction})`)
            return true
        }

        message.react("✅").catch(console.error)


        const timeUnavailableInSeconds = dirX !== 0 && dirY !== 0 ? 140 : 100
        player.currentAction = "travelling"
        player.currentActionData = {travelLocation, direction, message}
        player.currentActionDescription = `${player.name} is currently butchering a corpse`
        player.currentActionDuration = timeUnavailableInSeconds
        player.currentActionEndsAt = Date.now() + (1000 * timeUnavailableInSeconds)

        const embed = new MessageEmbed()
            .setColor('#ffffff')
            .setTitle(`${player.name} begins to travel [${direction.toUpperCase()}]!`)
            .setDescription(`${player.firstname} walks away to the ${direction.toLowerCase()} location. (ETA: ${timeUnavailableInSeconds}sec)`)

        await message.channel.send({embeds: [embed]})
        await message.delete().catch(console.error)
    }
)

travel.callbackGameLoop = async function (player, location, channel) {
    if (player?.currentAction === "travelling") {
        if (Date.now() >= player.currentActionEndsAt) {

            const guild = DiscordService.bot.guilds.cache.find(g => g.id.toString() === process.env.DISCORD_SERVER_ID)
            if (!guild) throw new Error('Guild not found')

            if (!WorldMapManager.getLocationByPosition(player?.currentActionData?.travelLocation?.positionX, player?.currentActionData?.travelLocation?.positionY)) {
                await channel.send({
                    embeds: [
                        new MessageEmbed()
                            .setColor(Colors.GOLD)
                            .setTitle(`${player.name} was travelling ${player.currentActionData.direction}, but encountered an invisible wall!`)
                            .setDescription(`${player.firstname} is unable to progress further in that direction...`)
                    ]
                })

                player.currentAction = null
                player.currentActionData = null
                player.currentActionEndsAt = null
                player.currentActionDuration = null
                player.currentActionDescription = null
                return true
            }

            const newChannelNamePos = WorldMapManager.getStringLocation(player?.currentActionData.travelLocation)
            const oldChannelNamePos = WorldMapManager.getStringLocation(location)

            const newChannel = await guild.channels.cache.find(c => c.name === newChannelNamePos)
            if (!newChannel) {
                await channel.send({
                    embeds: [
                        new MessageEmbed()
                            .setColor(Colors.GOLD)
                            .setTitle(`${player.name} was travelling ${player.currentActionData.direction}, but encountered an invisible wall!`)
                            .setDescription(`${player.firstname} is unable to progress further in that direction because channel couldn't be found... (this is a bug)`)
                    ]
                })
                player.currentAction = null
                player.currentActionData = null
                player.currentActionEndsAt = null
                player.currentActionDuration = null
                player.currentActionDescription = null
                return true
            }

            let newRole = guild.roles.cache.find(r => r.name === newChannelNamePos)
            if (!newRole) {
                newRole = await guild.roles
                    .create({
                        name: newChannelNamePos,
                        data: {
                            name: newChannelNamePos,
                            color: 'GREEN',
                        },
                        reason: 'location',
                    })
                    .catch(console.error)

                await newChannel.permissionOverwrites.create(newRole.id, {VIEW_CHANNEL: true}).catch(console.error);
            }

            const userCurrentLocationRole = player?.currentActionData?.message?.member?.roles?.cache?.find(r => r.name === oldChannelNamePos)


            if (!userCurrentLocationRole) {
                await channel.send({
                    embeds: [
                        new MessageEmbed()
                            .setColor(Colors.GOLD)
                            .setTitle(`${player.name} was travelling ${player.currentActionData.direction}, but encountered an invisible wall!`)
                            .setDescription(`${player.firstname} is unable to progress further in that direction because channel couldn't be found... (this is a bug)`)
                    ]
                })
                player.currentAction = null
                player.currentActionData = null
                player.currentActionEndsAt = null
                player.currentActionDuration = null
                player.currentActionDescription = null
                return true
            }

            await player.currentActionData.message.member.roles.remove(userCurrentLocationRole).catch(console.error)
            await player.currentActionData.message.member.roles.add(newRole).catch(console.error)

            player.location = WorldMapManager.getStringLocation(player.currentActionData.travelLocation)

            const embed = new MessageEmbed()
                .setColor(Colors.GOLD)
                .setTitle(`${player.name} arrived at ${player.currentActionData.travelLocation.name}!`)

            embed.setDescription(`${player.firstname} arrived in the new location!`)

            await newChannel.send({embeds: [embed]})

            await channel.send({
                embeds: [new MessageEmbed()
                    .setColor(Colors.GOLD)
                    .setTitle(`${player.name} has left!`)
                    .setDescription(`${player.firstname} had left in the ${player.currentActionData.direction} direction...`)]
            })

            player.currentAction = null
            player.currentActionData = null
            player.currentActionEndsAt = null
            player.currentActionDuration = null
            player.currentActionDescription = null
            SavingService.save()
        }
    }
}


export default travel