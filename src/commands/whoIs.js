import UserCommand from "./UserCommand.js"
import SavingService from "../util/SavingService.js"
import PlayerService from "../player/PlayerService.js"


const whoIs = new UserCommand(
    /^!wh?o?i?s?/i,
    async (message, player, location) => {

        const fullCommand = message.cleanContent.match(/^!wh?o?i?s? ?([0-9]*)?/i)

        if (!fullCommand[1]) {
            await message.react("❌").catch(console.error)
            await message.reply(`Bad usage. Use like this: \`!whoIs <USER_ID>\``)
            return true
        }

        const otherPlayer = PlayerService.getPlayer(fullCommand[1])

        if (!otherPlayer) {
            await message.react("❌").catch(console.error)
            await message.reply(`User with ID ${fullCommand[1]} can't be found.`)
            return true
        }

        await message.reply(`${otherPlayer.name}`)

    }
)

whoIs.actionBypass = true

export default whoIs