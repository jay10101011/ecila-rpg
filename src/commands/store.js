import UserCommand from "./UserCommand.js"
import {MessageEmbed} from "discord.js"
import WorldMapManager from "../worldmap/WorldMapManager.js"


const store = new UserCommand(
    /^!store/i,
    async (message, player, location, building) => {
        if (!building) {
            await message.react("❌").catch(console.error)
            await message.reply(`${player.name} tried to store something, but there isn't any storage here.`)
            return true
        }

        const fullCommand = message.cleanContent.match(/^!store ?(gold)? ?([0-9]*)?/i)

        if (fullCommand[1] && fullCommand[1].toLowerCase().trim() === 'gold') {
            let goldAmount = fullCommand[2] ? parseInt(fullCommand[2]) : null

            if (goldAmount === null){
                await message.react("❌").catch(console.error)
                await message.reply(`Bad usage. Use like this: \`!store gold 1000\``)
                return true
            }

            if (goldAmount <= 0){
                await message.react("❌").catch(console.error)
                await message.reply(`Can't store less than 1 gold.`)
                return true
            }

            if (goldAmount >= player.gold) {
                goldAmount = player.gold
            }

            if (building.gold === null) {
                building.gold = 0
            }
            building.gold += goldAmount
            player.gold -= goldAmount

            const embed = new MessageEmbed()
                .setColor('#665500')
                .setTitle(`${player.name} stored ${goldAmount} gold!`)
                .setDescription(`${player.firstname} stored ${goldAmount} gold in this building!`)

            await message.channel.send({embeds: [embed]})
        } else {
            if (fullCommand[2] && isNaN(parseInt(fullCommand[2]))) {
                await message.react("❌").catch(console.error)
                await message.reply(`Bad usage. Use like this: \`!store 0\``)
                return true
            }

            if (building.storage.length >= building.storageSize) {
                await message.react("❌").catch(console.error)
                await message.reply(`Cannot store any more items (storage full)`)
                return true
            }

            const slot = fullCommand[2] ? parseInt(fullCommand[2]) : (player.inventory.length - 1)

            if (slot >= player.inventory.length || slot < 0) {
                await message.react("❌").catch(console.error)
                await message.reply(`Cannot store item in slot [${slot}] (slot empty or not found)`)
                return true
            }

            const item = player.inventory[slot]

            if (building.storage === null) {
                building.storage = []
            }
            building.storage.push(item)
            player.inventory.splice(slot, 1)

            const embed = new MessageEmbed()
                .setColor('#665500')
                .setTitle(`${player.name} stored the item [${WorldMapManager.getItemText(item)}]!`)
                .setDescription(`${player.firstname} stored the item [${WorldMapManager.getItemText(item)}] in slot [${building.storage.length - 1}]`)

            await message.channel.send({embeds: [embed]})
        }

        await message.delete().catch(console.error)
    }
)

export default store