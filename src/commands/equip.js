import UserCommand from "./UserCommand.js"
import SavingService from "../util/SavingService.js"
import {MessageEmbed} from "discord.js"
import WorldMapManager from "../worldmap/WorldMapManager.js"


const equip = new UserCommand(
    /^!eq?u?i?p?/i,
    async (message, player, location) => {

        const fullCommand = message.cleanContent.match(/^!eq?u?i?p? ?((ac)c?e?s?s?o?r?y?|(ar)m?o?r?|(w)e?a?p?o?n?) ?([0-9]*)?/i)

        if (!(fullCommand?.[1])) {
            await message.react("❌").catch(console.error)
            await message.reply(`Bad usage. Use like this: \`!equipWeapon 0\``)
            return true
        }

        if (fullCommand[5] && isNaN(parseInt(fullCommand[5]))) {
            await message.react("❌").catch(console.error)
            await message.reply(`Bad usage. Use like this: \`!equipWeapon 0\``)
            return true
        }

        const slot = fullCommand[5] ? parseInt(fullCommand[5]) : (player.inventory.length - 1)

        if (slot >= player.inventory.length || slot < 0) {
            await message.react("❌").catch(console.error)
            await message.reply(`Cannot equip item in slot [${slot}] (slot empty or not found)`)
            return true
        }

        message.react("✅").catch(console.error)

        let title = ``
        let description = ``
        if (fullCommand[1].toLowerCase().startsWith('w')) {
            if (player.weapon) {
                const oldWeapon = player.weapon
                const newWeapon = player.inventory[slot]

                player.weapon = newWeapon
                player.inventory.splice(slot, 1, oldWeapon)

                title = `${player.name} equips ${newWeapon.name} as weapon!`
                description = `${player.firstname} equips the item **[${WorldMapManager.getItemText(player.weapon)}]** as weapon and puts the previous **[${WorldMapManager.getItemText(oldWeapon)}]** in inventory slot **[${slot}]**`
            } else {
                player.weapon = player.inventory[slot]
                player.inventory.splice(slot, 1)

                title = `${player.name} equips item ${player.weapon.name} as weapon`
                description = `${player.firstname} equips the item **[${WorldMapManager.getItemText(player.weapon)}]**`
            }
        } else if (fullCommand[1].toLowerCase().startsWith('ar')) {
            if (player.armor) {
                const oldWeapon = player.armor
                const newWeapon = player.inventory[slot]

                player.armor = newWeapon
                player.inventory.splice(slot, 1, oldWeapon)

                title = `${player.name} equips ${newWeapon.name} as armor!`
                description = `${player.firstname} equips the item **[${WorldMapManager.getItemText(player.armor)}]** as armor and puts the previous **[${WorldMapManager.getItemText(oldWeapon)}]** in inventory slot **[${slot}]**`
            } else {
                player.armor = player.inventory[slot]
                player.inventory.splice(slot, 1)

                title = `${player.name} equips item ${player.armor.name} as armor`
                description = `${player.firstname} equips the item **[${WorldMapManager.getItemText(player.armor)}]**`
            }
        } else if (fullCommand[1].toLowerCase().startsWith('ac')) {
            if (player.accessory) {
                const oldWeapon = player.accessory
                const newWeapon = player.inventory[slot]

                player.accessory = newWeapon
                player.inventory.splice(slot, 1, oldWeapon)

                title = `${player.name} equips ${newWeapon.name} as accessory!`
                description = `${player.firstname} equips the item **[${WorldMapManager.getItemText(player.accessory)}]** as accessory and puts the previous **[${WorldMapManager.getItemText(oldWeapon)}]** in inventory slot **[${slot}]**`
            } else {
                player.accessory = player.inventory[slot]
                player.inventory.splice(slot, 1)

                title = `${player.name} equips item ${player.accessory.name} as accessory`
                description = `${player.firstname} equips the item **[${WorldMapManager.getItemText(player.accessory)}]**`
            }
        }

        const embed = new MessageEmbed()
            .setColor('#665500')
            .setTitle(title)
            .setDescription(description)
            .addField('Equipped weapon', !player.weapon ? 'No Weapon' : `[${player.weapon.rarity} ${player.weapon.type}] ${player.weapon.name}`, true)
            .addField('Equipped armor', !player.armor ? 'No Armor' : `[${player.armor.rarity} ${player.armor.type}] ${player.armor.name}`, true)
            .addField('Equipped accessory', !player.accessory ? 'No accessory' : `[${player.accessory.rarity} ${player.accessory.type}] ${player.accessory.name}`, true)

        await message.channel.send({embeds: [embed]})

        await message.delete().catch(console.error)
        SavingService.save()

    }
)


export default equip