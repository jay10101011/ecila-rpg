import UserCommand from "./UserCommand.js"
import {MessageEmbed} from "discord.js"
import SavingService from "../util/SavingService.js"
import Colors from "../util/Colors.js"
import BuildingType from "../worldmap/BuildingType.js"


const mine = new UserCommand(
    /^!mine/i,
    async (message, player, location, building) => {

        if (building && building.type !== BuildingType.MINE) {
            await message.react("❌").catch(console.error)
            await message.reply(`${player.name} tried to mine, but there is no mine here!`)
            return true
        }

        const fullCommand = message.cleanContent.match(/^!mine ?([0-9]*)?/i)

        if (fullCommand[1] && isNaN(parseInt(fullCommand[1]))) {
            await message.react("❌").catch(console.error)
            await message.reply(`Bad usage. Use like this: \`!mine 10\``)
            return true
        }

        message.react("✅").catch(console.error)

        let miningTime = fullCommand[1] ? parseInt(fullCommand[1]) : 10

        if (miningTime < 1) {
            await message.react("❌").catch(console.error)
            await message.reply(`You can't mine for less than one minute!`)
            return true
        }

        if (!location.city?.mine) {
            await message.reply(`${player.name} tried to mine, but there is no mine here!`)
            return true
        }

        if (miningTime > 60 * 12) {
            miningTime = 60 * 12
        }

        const timeUnavailableInSeconds = miningTime * 60
        player.currentAction = "mining"
        player.currentActionData = miningTime
        player.currentActionDescription = `${player.name} is currently mining.`
        player.currentActionDuration = timeUnavailableInSeconds
        player.currentActionEndsAt = Date.now() + (1000 * timeUnavailableInSeconds)

        const embed = new MessageEmbed()
            .setColor('#ffffff')
            .setTitle(`${player.name} goes to the mine!`)
            .setDescription(`${player.firstname} is heading to the mine to extract some gold. (${Math.ceil(timeUnavailableInSeconds / 60)} minutes left)`)

        await message.channel.send({embeds: [embed]})
        await message.delete().catch(console.error)
    }
)


mine.callbackGameLoop = async function (player, location, channel) {
    if (player?.currentAction === "mining" && Date.now() >= player.currentActionEndsAt) {

        const miningTime = player.currentActionData ?? 10

        const goldAmount = Math.floor(Math.random() * (1 + (miningTime * 2)))
        const taxes = location?.city?.mine?.tax ? Math.ceil(goldAmount * location.city.mine.tax) : 0

        player.gold += goldAmount - taxes

        if (location?.city?.mine) {
            location.city.mine.gold += taxes
        }

        await channel.send({
            embeds: [
                new MessageEmbed()
                    .setColor(Colors.GOLD)
                    .setTitle(`${player.name} finished mining!`)
                    .setDescription(`${player.firstname} mined **${goldAmount} gold** in ${miningTime} minutes! (success: ${Math.round((goldAmount / (miningTime * 2)) * 200)}%)`)
                    .addField(`Mined gold`, `${goldAmount}`)
                    .addField(`Taxes`, `${taxes} gold (${(location?.city?.mine?.tax ? location.city.mine.tax * 100 : 0).toFixed(2)}%)`)
                    .addField(`Profit`, `${goldAmount - taxes} gold`)
                    .addField(`Total Character Gold`, `${player.gold}`)
            ]
        })

        player.currentAction = null
        player.currentActionData = null
        player.currentActionEndsAt = null
        player.currentActionDuration = null
        player.currentActionDescription = null
        SavingService.save()
    }
}

export default mine