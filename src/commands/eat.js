import UserCommand from "./UserCommand.js"
import {MessageEmbed} from "discord.js"
import GeneratorService from "../generator/GeneratorService.js"
import BaseGameService from "../generator/BaseGameService.js"
import WorldMapManager from "../worldmap/WorldMapManager.js"
import PlayerGender from "../player/PlayerGender.js"
import Const from "../Const.js"
import SavingService from "../util/SavingService.js"
import Colors from "../util/Colors.js"


async function eatItem(message, player, slot) {
    const prevWounds = [...new Set(player.health.wounds)].join(', ') || 'none'
    const prevStatus = player.health.status
    const prevBloodLoss = player.health.bloodLoss
    const item = player.inventory[slot]
    const itemText = WorldMapManager.getItemText(item)

    const objHeal = await GeneratorService.newWorkflow(
        BaseGameService.getGenerators(),
        BaseGameService.getWorkflows(),
        "eatHealEffects",
        [
            {
                name: "playerName",
                value: `${player.name} (${PlayerGender.fromIndex(player.gender)} ${player.race})`
            },
            {name: "wounds", value: prevWounds},
            {name: "status", value: prevStatus},
            {name: "bloodLoss", value: prevBloodLoss},
            {name: "item", value: itemText},
        ]
    )

    const textEnd = `${objHeal.description}`

    if (objHeal?.updatedWounds)
        player.health.wounds = (objHeal?.updatedWounds)?.split(',').map(w => w.trim()).filter(w => !['none', 'healed', 'cured'].some(e => w.toLowerCase().includes(e)))
    if (objHeal?.updatedStatus)
        player.health.status = objHeal.updatedStatus
    if (objHeal?.updatedBloodLoss)
        player.health.bloodLoss = objHeal.updatedBloodLoss

    if (Const.STATUS_DEAD.includes(player.health.status.trim().toLowerCase())) {
        player.isAlive = false
    }

    player.inventory.splice(slot, 1)

    const embed = new MessageEmbed()
        .setColor('#ffffff')
        .setTitle(`${player.name} tries to eat/drink [${itemText}]`)
        .setDescription(`${textEnd}`)

    embed.addField(`Previous status`, prevStatus || '[undefined]', true)
    embed.addField(`Updated status`, objHeal.updatedStatus || '[undefined]', true)
    embed.addField(`Previous blood loss`, prevBloodLoss || '[undefined]', true)
    embed.addField(`Updated blood loss`, objHeal.updatedBloodLoss || '[undefined]', true)
    embed.addField(`Previous wounds`, prevWounds || '[undefined]', true)
    embed.addField(`Updated wounds`, objHeal.updatedWounds || '[undefined]', true)

    await message.channel.send({embeds: [embed]})

    if (!player.isAlive) {
        await message.channel.send({
            embeds: [
                new MessageEmbed()
                    .setColor(Colors.BLACK)
                    .setTitle(`${player.name} is Dead!`)
                    .setDescription(`${player.firstname} succumbed after eating/drinking [${itemText}]!`)
            ]
        })
    }
}

const eat = new UserCommand(
    /^!eat/i,
    async (message, player, location) => {

        const fullCommand = message.cleanContent.match(/^!eat ?([0-9]*)? ?(f?)o?r?c?e?/i)

        if (fullCommand[1] && isNaN(parseInt(fullCommand[1]))) {
            await message.react("❌").catch(console.error)
            await message.reply(`Bad usage. Use like this: \`!eat 0\``)
            return true
        }

        const slot = fullCommand[1] ? parseInt(fullCommand[1]) : (player.inventory.length - 1)

        if (slot >= player.inventory.length || slot < 0) {
            await message.react("❌").catch(console.error)
            await message.reply(`Cannot eat item in slot [${slot}] (slot empty or not found)`)
            return true
        }

        message.react("✅").catch(console.error)


        if (fullCommand[2]) {
            await eatItem(message, player, slot)
        } else {
            const item = player.inventory[slot]
            const itemText = WorldMapManager.getItemText(item)

            const objEdible = await GeneratorService.newWorkflow(
                BaseGameService.getGenerators(),
                BaseGameService.getWorkflows(),
                "eat",
                [
                    {name: "item", value: itemText},
                    {name: "edible"},
                    {name: "effect"}
                ]
            )

            const isEdible = objEdible?.edible?.toLowerCase?.().trim() === 'yes'
            const isHeal = objEdible?.effect?.toLowerCase?.().trim() === 'heal'
            const isBuff = objEdible?.effect?.toLowerCase?.().trim() === 'buff'
            const isPoison = objEdible?.effect?.toLowerCase?.().trim() === 'poison'

            if (isEdible && (isHeal || isBuff)) {
                await eatItem(message, player, slot)
            } else {
                const embed = new MessageEmbed()
                    .setColor('#ffffff')
                    .setTitle(`${player.name} tries to eat/drink [${itemText}]`)
                    .setDescription(`${player.firstname} bites into the ${item.name}... But the item doesn't taste like it's edible.`)
                await message.channel.send({embeds: [embed]})
            }
        }


        await message.delete().catch(console.error)
    }
)

eat.callbackGameLoop = async function (player, location, channel) {
    if (player?.currentAction === "eating") {
        if (Date.now() >= player.currentActionEndsAt) {

            const embed = new MessageEmbed()
                .setColor(Colors.GOLD)
                .setTitle(`${player.name} finished butchering ${player.currentActionData.name}!`)


            embed.setDescription(`${player.firstname} has successfully extracted an item!`)

            await channel.send({embeds: [embed]})

            player.currentAction = null
            player.currentActionData = null
            player.currentActionEndsAt = null
            player.currentActionDuration = null
            player.currentActionDescription = null
            SavingService.save()
        }
    }
}

export default eat