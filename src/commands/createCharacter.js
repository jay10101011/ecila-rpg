import UserCommand from "./UserCommand.js"
import GeneratorService from "../generator/GeneratorService.js"
import BaseGameService from "../generator/BaseGameService.js"
import Player from "../player/Player.js"
import PlayerGender from "../player/PlayerGender.js"
import PlayerService from "../player/PlayerService.js"
import {MessageEmbed} from "discord.js"
import Const from "../Const.js"
import SavingService from "../util/SavingService.js"
import Colors from "../util/Colors.js"

const createCharacter = new UserCommand(
    /^!createChar(acter)?/i,
    async (message, player, location) => {
        const correctFullCommand = message.cleanContent.match(/^!createChar(acter)? (male|female) ?(ke?e?p?)?/i)
        const gender = correctFullCommand?.[2]

        if (!gender) {
            message.reply(`You need to provide a gender, either \`male\` or \`female\``)
        } else {
            message.react("✅").catch(console.error)

            const args = [
                {name: "gender", value: gender},
                {name: "firstname"},
                {name: "lastname"},
            ]

            const object = await GeneratorService.newWorkflow(BaseGameService.getGenerators(), BaseGameService.getWorkflows(), "createPlayer", args)


            const oldName = PlayerService.players[message.author.id]?.name
            const oldFirstname = PlayerService.players[message.author.id]?.firstname
            const oldLastname = PlayerService.players[message.author.id]?.lastname

            const player = new Player(message.author.id.toString(), object.firstname, object.lastname, PlayerGender.getIndex(gender))

            const member = await message.member.fetch(true).catch(console.error)

            if (PlayerService.players[message.author.id] && correctFullCommand[3]?.startsWith?.('k')) {
                player.name = oldName
                player.firstname = oldFirstname
                player.lastname = oldLastname

                member.roles.cache.map(async r => {
                    if (r.name.startsWith('loc-')) {
                        await member.roles.remove(r)
                    }
                })
            }
            PlayerService.players[message.author.id] = player

            const embed = new MessageEmbed()
                .setColor(Colors.GOLD)
                .setTitle(`Character Creation`)
                .setDescription(`${message.author} created the character ${player.name}, a ${PlayerGender.fromIndex(player.gender)} human!`)
                .addField("Firstname", player.firstname, true)
                .addField("Lastname", player.lastname, true)
                .addField("Gender", PlayerGender.fromIndex(player.gender), true)
                .addField("Race", player.race, true)
                .addField("Current location", player.location, false)

            const role = message.guild.roles.cache.find(r => r.name === Const.ROLE_PLAYER)
            if (!role) console.error(`No starting player role found!`)

            const roleLocation = message.guild.roles.cache.find(r => r.name === `loc-00-00`)
            if (!roleLocation) console.error(`No starting location role found!`)

            await member.roles.add(role).catch(console.error)
            await member.roles.add(roleLocation).catch(console.error)


            await message.channel.send({embeds: [embed]})

            await message.delete().catch(console.error)

            SavingService.save()
        }
    },
    true
)

createCharacter.actionBypass = true

export default createCharacter