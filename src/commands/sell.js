import UserCommand from "./UserCommand.js"
import SavingService from "../util/SavingService.js"
import GeneratorService from "../generator/GeneratorService.js"
import BaseGameService from "../generator/BaseGameService.js"
import {MessageEmbed} from "discord.js"
import WorldMapManager from "../worldmap/WorldMapManager.js"
import Colors from "../util/Colors.js"


async function getPrice(item) {
    const args = [
        {name: "item", value: item.name},
        {name: "type", value: item.type || 'undefined type'},
        {name: "rarity", value: item.rarity || 'undefined rarity'},
        {name: "price"},
    ]

    const object = await GeneratorService.newWorkflow(
        BaseGameService.getGenerators(),
        BaseGameService.getWorkflows(),
        "sell",
        args
    )

    const goldAmountString = object.price?.toLowerCase().trim().split(' ')[0]
    let goldAmount = !object?.price ? null : parseInt(
        goldAmountString
    )


    if (['infinite', 'unlimited', 'unknown', 'infinity', '-infinity', 'unlimited!', 'infinity!']
        .includes(goldAmountString)) {
        return 100000
    }

    if (isNaN(parseInt(goldAmountString))) {
        // RETRY
    }

    return goldAmount
}

const sell = new UserCommand(
    /^!sel?l?/i,
    async (message, player, location) => {

        return await message.reply(`Sorry, this command is disabled until further features are implemented! If you want money, use the \`!mine\` command`)

        const fullCommand = message.cleanContent.match(/^!sel?l? ?(al?l?)? ?([0-9]*)?/i)
        let itemIndexes = message.cleanContent.match(/( *\d*)*$/ig)?.[0]
            ?.split?.(/[, ]/)
            ?.map(s => s.trim())
            ?.map(s => isNaN(parseInt(s)) ? null : parseInt(s))
            ?.filter(s => s !== null)
        itemIndexes = [...new Set(itemIndexes)]


        if (fullCommand[2] && isNaN(parseInt(fullCommand[2]))) {
            await message.react("❌").catch(console.error)
            await message.reply(`Bad usage. Use like this: \`!sell 0\``)
            return true
        }

        const slot = fullCommand[2] ? parseInt(fullCommand[2]) : (player.inventory.length - 1)

        if (slot >= player.inventory.length || slot < 0) {
            await message.react("❌").catch(console.error)
            await message.reply(`Cannot sell item in slot [${slot}] (slot empty or not found)`)
            return true
        }

        if (itemIndexes.some(i => isNaN(i))) {
            await message.react("❌").catch(console.error)
            await message.reply(`Cannot sell one or more items (slot empty or not found)`)
            return true
        }

        message.react("✅").catch(console.error)

        const items = fullCommand[1] ?
            [...player.inventory]
            : fullCommand[2] ?
                itemIndexes.map(i => player.inventory[i])
                : [player.inventory[slot]]

        const timeUnavailableInSeconds = 30
        player.currentAction = "selling an item"
        player.currentActionData = items
        player.currentActionDescription = null
        player.currentActionDuration = timeUnavailableInSeconds
        player.currentActionEndsAt = Date.now() + (1000 * timeUnavailableInSeconds)

        const embed = new MessageEmbed()
            .setColor('#ffff00')
            .setTitle(`${player.name} is going to sell items!`)
            .setDescription(`${player.firstname} walks to the nearby merchant to sell ${items.length} item${items.length ? 's' : ''} (${timeUnavailableInSeconds}sec left)`)

        await message.channel.send({embeds: [embed]})

        await message.delete().catch(console.error)
        SavingService.save()
    }
)

async function sellItem(player, item, channel) {
    if (!item) {
        await channel.send(`${player.name} tried to sell an item but something went wrong!`)
        return `Something went wrong when trying to sell (this is a bug)`
    }

    let itemPrice = await getPrice(item)
    let counter = 1
    while (itemPrice === null || isNaN(itemPrice)) {
        itemPrice = await getPrice(item)

        counter++
        if (counter >= 5) {
            break
        }
    }

    if (itemPrice === null || isNaN(itemPrice)) {
        await channel.send(`${player.name} tried to sell an item but the merchant refused to buy it. (missing numerical value in result, tried 5 times)`)
        return `Something went wrong when trying to sell [${WorldMapManager.getItemText(item)}] (missing numerical value in result, tried 5 times)`
    }

    const itemIndex = player.inventory.indexOf(item)
    if (itemIndex === -1) {
        await channel.send({
            embeds: [
                new MessageEmbed()
                    .setColor(Colors.GOLD)
                    .setTitle(`${player.name} was going to sell an item, but it looks like this item isn't there anymore!`)
                    .setDescription(`The item couldn't be found...`)
            ]
        })
        player.currentAction = null
        player.currentActionData = null
        player.currentActionEndsAt = null
        player.currentActionDuration = null
        player.currentActionDescription = null
        return `Something went wrong when trying to sell [${WorldMapManager.getItemText(item)}] (item couldn't be found in player inventory)`
    }

    player.gold += itemPrice
    player.inventory.splice(itemIndex, 1)

    return `[${WorldMapManager.getItemText(item)}] sold for **${itemPrice} gold**`

}

sell.callbackGameLoop = async function (player, location, channel) {
    if (player?.currentAction === "selling an item") {
        if (Date.now() >= player.currentActionEndsAt) {

            if (!player.currentActionData || !player.currentActionData.length) {
                await channel.send({
                    embeds: [
                        new MessageEmbed()
                            .setColor(Colors.GOLD)
                            .setTitle(`${player.name} was going to sell an item, but it looks like something went wrong!`)
                            .setDescription(`Some items couldn't be found...`)
                    ]
                })
                player.currentAction = null
                player.currentActionData = null
                player.currentActionEndsAt = null
                player.currentActionDuration = null
                player.currentActionDescription = null
                return true
            }

            const resultLines = []
            for (let item of player.currentActionData) {
                resultLines.push(await sellItem(player, item, channel))
            }

            await channel.send({
                embeds: [new MessageEmbed()
                    .setColor(Colors.GOLD)
                    .setTitle(`${player.name} sold some items!`)
                    .setDescription(`${resultLines.filter(l => l !== null).join(`\n`)}`)]
            })

            player.currentAction = null
            player.currentActionData = null
            player.currentActionEndsAt = null
            player.currentActionDuration = null
            player.currentActionDescription = null
            SavingService.save()
        }
    }
}

export default sell