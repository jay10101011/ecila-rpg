import {config} from "dotenv"
import UserCommand from "./UserCommand.js"
import {MessageEmbed} from "discord.js"
import WorldMapManager from "../worldmap/WorldMapManager.js"
import DiscordService from "../util/DiscordService.js"
import Colors from "../util/Colors.js"

config()

const enter = new UserCommand(
    /^!enter/i,
    async (message, player, location) => {
        const channel = message.channel
        const fullCommand = message.cleanContent.match(/^!enter ?([0-9]*)/i)

        if (!fullCommand[1] || (fullCommand[1] && isNaN(parseInt(fullCommand[1])))) {
            await message.react("❌").catch(console.error)
            await message.reply(`Bad usage. Use like this: \`!enter 0\``)
            return true
        }

        const plotId = parseInt(fullCommand[1])

        const building = WorldMapManager.getBuildingByPosition(location.positionX, location.positionY, plotId)

        if (!building) {
            await message.react("❌").catch(console.error)
            await message.reply(`It looks like there is nothing here!`)
            return true
        }

        if (!player.keys?.includes(building.key)) {
            await message.react("❌").catch(console.error)
            await message.reply(`It looks like ${player.name} doesn't have the key to enter here!`)
            return true
        }

        const plotNamePos = `loc-${location.positionX.toString().padStart(2, '0')}-${location.positionY.toString().padStart(2, '0')}-${plotId.toString().padStart(3, '0')}`
        const guild = DiscordService.bot.guilds.cache.find(g => g.id.toString() === process.env.DISCORD_SERVER_ID)

        const oldChannelNamePos = player.location

        const newChannel = await guild.channels.cache.find(c => c.name === plotNamePos)
        if (!newChannel) {
            await message.react("❌").catch(console.error)
            await channel.send({
                embeds: [
                    new MessageEmbed()
                        .setColor(Colors.GOLD)
                        .setTitle(`${player.name} tried to enter a plot of land, but encountered an invisible wall!`)
                        .setDescription(`${player.firstname} is unable to progress further in that direction because channel couldn't be found... (this is a bug)`)
                ]
            })
            return true
        }

        let newRole = guild.roles.cache.find(r => r.name === plotNamePos)
        if (!newRole) {
            await message.react("❌").catch(console.error)
            await message.reply(`Role ${plotNamePos} couldn't be found. (this is a bug)`)
            return true
        }

        const userCurrentLocationRole = message.member?.roles?.cache?.find(r => r.name === oldChannelNamePos)

        if (!userCurrentLocationRole) {
            await message.react("❌").catch(console.error)
            await channel.send({
                embeds: [
                    new MessageEmbed()
                        .setColor(Colors.GOLD)
                        .setTitle(`${player.name} tried to enter a plot of land, but encountered an invisible wall!`)
                        .setDescription(`${player.firstname} is unable to progress further in that direction because role couldn't be found... (this is a bug)`)
                ]
            })
            return true
        }

        message.react("✅").catch(console.error)

        await message.member.roles.remove(userCurrentLocationRole).catch(console.error)
        await message.member.roles.add(newRole).catch(console.error)

        player.location = plotNamePos

        const embed = new MessageEmbed()
            .setColor('#ffffff')
            .setTitle(`${player.name} enters the plot of land [${plotId}]`)
            .setDescription(`${player.firstname} uses ${player.gender?`her`:'his'} key to enter.`)

        await message.channel.send({embeds: [embed]})

        await newChannel.send({embeds: [new MessageEmbed()
                .setColor('#ffffff')
                .setTitle(`${player.name} arrived!`)
                .setDescription(`${player.firstname} just arrived.`)]})

        await message.delete().catch(console.error)
    }
)

export default enter