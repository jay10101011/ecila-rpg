import UserCommand from "./UserCommand.js"
import {MessageEmbed} from "discord.js"
import GeneratorService from "../generator/GeneratorService.js"
import BaseGameService from "../generator/BaseGameService.js"
import Item from "../entity/Item.js"
import WorldMapManager from "../worldmap/WorldMapManager.js"
import SavingService from "../util/SavingService.js"
import Colors from "../util/Colors.js"


const butcher = new UserCommand(
    /^!butcher/i,
    async (message, player, location, building) => {

        const corpse = (building ? building : location).enemyCorpses?.pop() || null

        if (!corpse) {
            await message.reply(`${player.name} tried to butcher an enemy, but there is no enemy corpse here!`)
            return true
        }

        message.react("✅").catch(console.error)

        const timeUnavailableInSeconds = 30
        player.currentAction = "butchering a corpse"
        player.currentActionData = corpse
        player.currentActionDescription = `${player.name} is currently butchering a corpse`
        player.currentActionDuration = timeUnavailableInSeconds
        player.currentActionEndsAt = Date.now() + (1000 * timeUnavailableInSeconds)

        const embed = new MessageEmbed()
            .setColor('#ffffff')
            .setTitle(`${player.name} butchers the corpse of the ${player.currentActionData.name}!`)
            .setDescription(`${player.firstname} begins to butcher the dead ${player.currentActionData.name} for parts. (${timeUnavailableInSeconds}sec left)`)

        await message.channel.send({embeds: [embed]})
        await message.delete().catch(console.error)
    }
)


/**
 * @param {Enemy} enemy
 * @return {Promise<Item>}
 */
async function getItem(enemy) {
    const object = await GeneratorService.newWorkflow(
        BaseGameService.getGenerators(),
        BaseGameService.getWorkflows(),
        "butcher",
        [
            {name: "name", value: `${enemy.name} (${enemy.type} ${enemy.difficulty})`},
            {name: "item"},
            {name: "type"},
            {name: "rarity"},
        ]
    )

    return new Item(object.rarity, object.type, object.item)
}

async function levelUp(player, channel) {
    if (player.butcheringLevel === null || player.butcheringLevel === undefined) player.butcheringLevel = 0
    if (Math.random() < 1 / (Math.pow(player.butcheringLevel + 1, 3))) {
        player.butcheringLevel++
        const embed = new MessageEmbed()
            .setColor(Colors.GOLD)
            .setTitle(`${player.name} butchering level increased!`)
            .setDescription(`${player.firstname} is now better at butchering enemy corpses!`)
            .addField("New butchering level", `${player.butcheringLevel}`, true)

        await channel.send({embeds: [embed]})
    }
}

butcher.callbackGameLoop = async function (player, location, channel, building) {
    if (player?.currentAction === "butchering a corpse") {
        if (Date.now() >= player.currentActionEndsAt) {

            const isAnimal = ["animal", "animals"].includes(player.currentActionData.type.toLowerCase())

            const itemMinNonAnimal = 0
            const itemMaxNonAnimal = 1 + (player.butcheringLevel || 0)
            const itemMinAnimal = 1
            const itemMaxAnimal = 2 + ((player.butcheringLevel || 0) / 2)
            const itemMin = isAnimal ? itemMinAnimal : itemMinNonAnimal
            const itemMax = (isAnimal ? itemMaxAnimal : itemMaxNonAnimal) + 1
            const nbItem = Math.floor(Math.random() * (itemMax - itemMin) + itemMin)

            const embed = new MessageEmbed()
                .setColor(Colors.GOLD)
                .setTitle(`${player.name} finished butchering ${player.currentActionData.name}!`)

            if (nbItem === 1)
                embed.setDescription(`${player.firstname} has successfully extracted an item!`)
            else if (nbItem === 0) {
                embed.setDescription(`But ${player.firstname} failed to retrieve any item...`)
            } else {
                embed.setDescription(`${player.firstname} has successfully extracted some items!`)
            }

            const loc = building ? building : location
            for (let i = 0; i < nbItem; i++) {
                const item = await getItem(player.currentActionData)
                loc.itemsOnTheFloor.push(item)
                embed.addField(`Loot`, `${WorldMapManager.getItemText(item)} [${loc.itemsOnTheFloor.length - 1}]`)
            }

            await channel.send({embeds: [embed]})

            await levelUp(player, channel)

            player.currentAction = null
            player.currentActionData = null
            player.currentActionEndsAt = null
            player.currentActionDuration = null
            player.currentActionDescription = null
            SavingService.save()
        }
    }
}

export default butcher