import {config} from "dotenv"
import UserCommand from "./UserCommand.js"
import {MessageEmbed} from "discord.js"
import WorldMapManager from "../worldmap/WorldMapManager.js"
import DiscordService from "../util/DiscordService.js"
import Colors from "../util/Colors.js"

config()

const exit = new UserCommand(
    /^!(leave|exit)/i,
    async (message, player, location) => {
        const channel = message.channel

        const building = WorldMapManager.getBuildingByString(player.location)

        if (!building) {
            await message.react("❌").catch(console.error)
            await message.reply(`You are not in a building right now!`)
            return true
        }

        const guild = DiscordService.bot.guilds.cache.find(g => g.id.toString() === process.env.DISCORD_SERVER_ID)

        const oldChannelNamePos = player.location

        const newLocation = WorldMapManager.getLocationByString(player.location)

        const newChannel = await guild.channels.cache.find(c => c.name === WorldMapManager.getStringLocation(newLocation))
        if (!newChannel) {
            await message.react("❌").catch(console.error)
            await channel.send({
                embeds: [
                    new MessageEmbed()
                        .setColor(Colors.GOLD)
                        .setTitle(`${player.name} tried to exit a plot of land, but encountered an invisible wall!`)
                        .setDescription(`${player.firstname} is unable to progress further in that direction because channel couldn't be found... (this is a bug)`)
                ]
            })
            return true
        }

        let newRole = guild.roles.cache.find(r => r.name === WorldMapManager.getStringLocation(newLocation))
        if (!newRole) {
            await message.react("❌").catch(console.error)
            await message.reply(`Role ${WorldMapManager.getStringLocation(newLocation)} couldn't be found. (this is a bug)`)
            return true
        }

        const userCurrentLocationRole = message.member?.roles?.cache?.find(r => r.name === oldChannelNamePos)
        if (!userCurrentLocationRole) {
            await message.react("❌").catch(console.error)
            await channel.send({
                embeds: [
                    new MessageEmbed()
                        .setColor(Colors.GOLD)
                        .setTitle(`${player.name} tried to exit a plot of land, but encountered an invisible wall!`)
                        .setDescription(`${player.firstname} is unable to progress further in that direction because role couldn't be found... (this is a bug)`)
                ]
            })
            return true
        }

        message.react("✅").catch(console.error)

        await message.member.roles.remove(userCurrentLocationRole).catch(console.error)
        await message.member.roles.add(newRole).catch(console.error)

        player.location = WorldMapManager.getStringLocation(newLocation)

        const embed = new MessageEmbed()
            .setColor('#ffffff')
            .setTitle(`${player.name} exits!`)
            .setDescription(`${player.firstname} decided to go outside.`)

        await message.channel.send({embeds: [embed]})

        await newChannel.send({embeds: [new MessageEmbed()
                .setColor('#ffffff')
                .setTitle(`${player.name} arrived!`)
                .setDescription(`${player.firstname} just arrived.`)]})

        await message.delete().catch(console.error)
    }
)

export default exit