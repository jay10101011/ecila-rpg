import UserCommand from "./UserCommand.js"
import {MessageEmbed} from "discord.js"
import PlayerService from "../player/PlayerService.js"

const look = new UserCommand(
    /^!look?/i,
    async (message, player, location, building) => {

        if (!building) {
            const cityText = `**City**: ${location.city?.name || 'None'}\n\n**Plots of land**:\n${location.city?.buildings?.map(b => `${b.name}: ${PlayerService.getPlayer(b.owner).name}`).join('\n')}\n\n`

            const items = location.itemsOnTheFloor
            const itemListString = `${items
                .map((item, i) => `${i}: [${((60) - ((Date.now() - item.droppedAt) / (1000 * 60))).toFixed(0)}min left] [${item.rarity} ${item.type}] ${item.name}`)
                .join('\n') || 'None'}`

            const corpseListString = location.enemyCorpses
                .map(c => `[${((60) - ((Date.now() - c.deadAt) / (1000 * 60))).toFixed(0)}min left] ${c.name} (${c.difficulty} ${c.type})`)
                .join('\n')
            const corpses = `**Corpses**:\n` + (corpseListString ? corpseListString : 'None')

            let chunks = []
            let lines = `${cityText}${corpses}\n\n**Items**:\n${itemListString}`.split('\n')
            for (let i = 0; i < lines.length; i++) {
                const line = lines[i]
                const messageWillBeTooLong = chunks.length === 0 || chunks[chunks.length - 1].length + line.length + 2 > 4000
                if (i % 50 === 0 || messageWillBeTooLong) {
                    chunks.push(line)
                } else {
                    chunks[chunks.length - 1] += "\n" + line
                }
            }

            const messages = chunks.map((c, i) => new MessageEmbed()
                .setColor('#0099ff')
                .setTitle(`What ${player.name} sees (page ${i + 1}/${chunks.length})`)
                .setDescription(c))

            for (let m of messages) {
                await message.channel.send({embeds: [m]})
            }
        } else {
            const buildingText = `**Building**: ${building.name || 'None'}\n\n`

            const items = building.itemsOnTheFloor || []
            const itemListString = `**Items on the floor**:\n${items
                .map((item, i) => `${i}: [${((60) - ((Date.now() - item.droppedAt) / (1000 * 60))).toFixed(0)}min left] [${item.rarity} ${item.type}] ${item.name}`)
                .join('\n') || 'None'}\n\n`

            const storedItems = building.storage || []
            const storedItemsListString = `**Stored Items (${building.storage ? building.storage.length : 0}/${building.storageSize})**:\n${storedItems
                .map((item, i) => `${i}: [${item.rarity} ${item.type}] ${item.name}`)
                .join('\n') || 'None'}`

            let chunks = []
            let lines = `${buildingText}${itemListString}${storedItemsListString}`.split('\n')
            for (let i = 0; i < lines.length; i++) {
                const line = lines[i]
                const messageWillBeTooLong = chunks.length === 0 || chunks[chunks.length - 1].length + line.length + 2 > 4000
                if (i % 50 === 0 || messageWillBeTooLong) {
                    chunks.push(line)
                } else {
                    chunks[chunks.length - 1] += "\n" + line
                }
            }

            const messages = chunks.map((c, i) => new MessageEmbed()
                .setColor('#0099ff')
                .setTitle(`What ${player.name} sees (page ${i + 1}/${chunks.length})`)
                .setDescription(c))

            for (let m of messages) {
                await message.channel.send({embeds: [m]})
            }
        }

        await message.delete().catch(console.error)
    }
)

look.actionBypass = true

export default look