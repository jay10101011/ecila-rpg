import UserCommand from "./UserCommand.js"
import {MessageEmbed} from "discord.js"
import GeneratorService from "../generator/GeneratorService.js"
import BaseGameService from "../generator/BaseGameService.js"
import Const from "../Const.js"
import PlayerService from "../player/PlayerService.js"
import PlayerGender from "../player/PlayerGender.js"
import WorldMapManager from "../worldmap/WorldMapManager.js"
import SavingService from "../util/SavingService.js"
import Colors from "../util/Colors.js"


/**
 *
 * @param message
 * @param {Player} player
 * @param {number} slot
 * @param {Player|Enemy} target
 * @param {Location} location
 * @return {Promise<void>}
 */
async function castSpellOnTarget(message, player, slot, target, location) {
    const prevWounds = [...new Set(target.health.wounds)].join(', ') || 'none'
    const prevStatus = target.health.status
    const prevBloodLoss = target.health.bloodLoss
    const spell = player.spells[slot]
    const wasAlive = target.isAlive

    const playerText = `[ Player: ${player.firstname} ${player.lastname}; gender: ${PlayerGender.fromIndex(player.gender) || "unspecified"}`
        + `; race: ${player.race}; spell: ${spell} ]`

    const targetText = target.userId ?
        (`[ Target: ${target.firstname} ${target.lastname}; gender: ${PlayerGender.fromIndex(target.gender) || "unspecified"}`
            + `; race: ${target.race}; armor: ${WorldMapManager.getItemText(target.armor)}`
            + `; accessory: ${WorldMapManager.getItemText(target.accessory)}; wounds: ${target.health.wounds.length > 0 ? target.health.wounds.join(', ') : 'None'}`
            + `; blood loss level: ${target.health.bloodLoss}; health status: ${target.health.status} ]`)
        : (`[ Target: ${target.name}; type: ${target.type}; difficulty: ${target.difficulty}`
            + `; armor: ${WorldMapManager.getItemText(target.armor)}`
            + `; accessory: ${WorldMapManager.getItemText(target.accessory)}; wounds: ${target.health.wounds.length > 0 ? target.health.wounds.join(', ') : 'None'}`
            + `; blood loss level: ${target.health.bloodLoss}; health status: ${target.health.status} ]`)

    const objSpellResult = await GeneratorService.newWorkflow(
        BaseGameService.getGenerators(),
        BaseGameService.getWorkflows(),
        "playerCastSpellToPlayer",
        [
            {
                name: "player",
                value: playerText
            },
            {name: "target", value: targetText},
            {name: "wounds", value: prevWounds},
            {name: "status", value: prevStatus},
            {name: "bloodLoss", value: prevBloodLoss},
        ]
    )

    const textEnd = `${objSpellResult.description}`

    if (objSpellResult?.updatedWounds)
        target.health.wounds = (objSpellResult?.updatedWounds)?.split(',').map(w => w.trim()).filter(w => !['none', 'healed', 'cured'].some(e => w.toLowerCase().includes(e)))
    if (objSpellResult?.updatedStatus)
        target.health.status = objSpellResult.updatedStatus
    if (objSpellResult?.updatedBloodLoss)
        target.health.bloodLoss = objSpellResult.updatedBloodLoss

    target.isAlive = !Const.STATUS_DEAD.includes(target.health.status.trim().toLowerCase())

    player.spells.splice(slot, 1)

    const embed = new MessageEmbed()
        .setColor('#ffffff')
        .setTitle(`**${player.name}** casts a spell on **${target === player ? player.gender ? 'herself' : 'himself' : target.name}**!`)
        .setDescription(`${textEnd}\n\nSpell: ${spell}`)

    embed.addField(`Previous status`, prevStatus || '[undefined]', true)
    embed.addField(`Updated status`, objSpellResult.updatedStatus || '[undefined]', true)
    embed.addField(`Previous blood loss`, prevBloodLoss || '[undefined]', true)
    embed.addField(`Updated blood loss`, objSpellResult.updatedBloodLoss || '[undefined]', true)
    embed.addField(`Previous wounds`, prevWounds || '[undefined]', true)
    embed.addField(`Updated wounds`, objSpellResult.updatedWounds || '[undefined]', true)

    await message.channel.send({embeds: [embed]})

    if (wasAlive && !target.isAlive) {
        let karmaGain = 0
        if (target.karma && target.karma < 0 && player.karma && player.karma >= 0) {
            karmaGain = 1
            target.karma += karmaGain
        }

        await PlayerService.enemyIsDead(target, player, karmaGain, message, location)
    }

    if (!wasAlive && target.isAlive) {
        if (target.currentAction === "resurrecting") {
            target.currentAction = null
            target.currentActionData = null
            target.currentActionDuration = null
            target.currentActionEndsAt = null
        }

        player.karma += 1

        await message.channel.send({
            embeds: [
                new MessageEmbed()
                    .setColor(Colors.GREEN)
                    .setTitle(`${target.name} was resurrected by ${player.name}!`)
                    .setDescription(`${player.firstname} gained **+1 karma** for resurrecting a player.`)
            ]
        })
    }

}

const castSpell = new UserCommand(
    /^!castS?p?e?l?l?/i,
    async (message, player, location, building) => {

        const fullCommand = message.cleanContent.match(/^!castS?p?e?l?l? ?([0-9]*)? ?([^\n]*)?/i)

        if (fullCommand[1] && isNaN(parseInt(fullCommand[1]))) {
            await message.react("❌").catch(console.error)
            await message.reply(`Bad usage. Use like this: \`!castSpell 0\``)
            return true
        }

        let characterName = fullCommand[2]?.trim?.()
        if (characterName && characterName.toLowerCase() === 'self') characterName = player.name
        const playerTarget = PlayerService.getPlayerByName(characterName)
        if (characterName && !playerTarget) {
            await message.reply(`${player.name} tried to cast a spell on ${characterName}, but the target couldn't be found!`)
            return true
        }

        const targetIsEnemy = !fullCommand[2]?.trim?.()

        const target = targetIsEnemy ? location.activeEnemy : playerTarget

        if (playerTarget && playerTarget.location !== player.location) {
            await message?.reply?.(`${player.name} tried to attack ${characterName.name}, but the target couldn't be found in this location!`)
            return true
        }

        if (!target) {
            await message.react("❌").catch(console.error)
            await message.reply(`Cannot find target (this is a bug)`)
            return true
        }

        if (!playerTarget && building) {
            await message.react("❌").catch(console.error)
            await message.reply(`Cannot attack monsters from inside a plot of land.`)
            return true
        }

        const slot = fullCommand[1] ? parseInt(fullCommand[1]) : (player.spells.length - 1)

        if (slot >= player.spells.length || slot < 0) {
            await message.react("❌").catch(console.error)
            await message.reply(`Cannot cast spell in slot [${slot}] (slot empty or not found)`)
            return true
        }

        message.react("✅").catch(console.error)

        await castSpellOnTarget(message, player, slot, target, location)

        await message.delete().catch(console.error)
        SavingService.save()
    }
)

export default castSpell