import UserCommand from "./UserCommand.js"
import {MessageEmbed} from "discord.js"
import Colors from "../util/Colors.js"
import GeneratorService from "../generator/GeneratorService.js"
import BaseGameService from "../generator/BaseGameService.js"

const Prices = {
    CITY: 20000,
}

const createCity = new UserCommand(
    /^!createCity/i,
    async (message, player, location, building) => {

        /*if (location.city) {
            await message.react("❌").catch(console.error)
            await message.reply(`${player.name} tried to create a city, but there is already a city in this location!`)
            return true
        }*/

        /*if (player.gold < Prices.CITY) {
            await message.react("❌").catch(console.error)
            await message.reply(`${player.name} tried to create a city, but doesn't have enough gold. (${player.gold}/${Prices.CITY} gold)`)
            return true
        }*/

        const object = await GeneratorService.newWorkflow(
            BaseGameService.getGenerators(),
            BaseGameService.getWorkflows(),
            "createCity",
            [
                {name: "difficulty", value: `${location.difficulty.join(', ')})`},
                {name: "type", value: `${location.enemiesTypes.join(', ')})`},
                {name: "city"},
                {name: "description"},
            ]
        )

        //player.gold -= Prices.CITY
        //location.city = new City(object.city)

        await message.channel.send({
            embeds: [new MessageEmbed()
                .setColor(Colors.GOLD)
                .setTitle(`${player.name} invested ${Prices.CITY} gold to found a city!`)
                .setDescription(`${player.firstname} founded the city ${object.city}!\n\nDescription: ${object.description}`)]
        })

        await message.delete().catch(console.error)
    }
)

export default createCity