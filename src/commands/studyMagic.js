import UserCommand from "./UserCommand.js"
import SavingService from "../util/SavingService.js"
import {MessageEmbed} from "discord.js"
import PlayerService from "../player/PlayerService.js"

const studyMagic = new UserCommand(
    /^!studyM?a?g?i?c?/i,
    async (message, player, location, building) => {

        if (!building){
            await message.react("❌").catch(console.error)
            await message.reply(`${player.name} tried to study, but isn't in a calm and safe environment... ${player.firstname} will have to go inside a building or plot of land to do that!`)
            return true
        }

        const timeUnavailableInSeconds = 300
        player.currentAction = "studying magic"
        player.currentActionData = null
        player.currentActionDescription = `${player.name} is currently studying magic.`
        player.currentActionDuration = timeUnavailableInSeconds
        player.currentActionEndsAt = Date.now() + (1000 * timeUnavailableInSeconds)

        await message.channel.send({
            embeds: [
                new MessageEmbed()
                    .setColor('#ffffff')
                    .setTitle(`${player.name} begins to study magic.`)
                    .setDescription(`${player.firstname} is now studying the fundamentals of magic. (${timeUnavailableInSeconds}sec left)`)
            ]
        })

        await message.delete().catch(console.error)
        SavingService.save()
    }
)

studyMagic.callbackGameLoop = async function (player, location, channel) {
    if (player?.currentAction === "studying magic" && Date.now() >= player.currentActionEndsAt) {

        const reply = await PlayerService.levelUpMagic(channel, player, 3) ?
            `${player.firstname} has learned new things about magic! ${player.firstname}'s efforts paid off, and ${player.gender ? 'she' : 'he'} reached the **magic level ${player.magicalLevel}**!`
            : `Unfortunately, ${player.gender ? 'she' : 'he'} didn't learn much more than ${player.gender ? 'she' : 'he'} currently knows... (1/${(Math.pow(player.magicalLevel + 1, 3))} chances of leveling up)`
        await channel.send({
            embeds: [
                new MessageEmbed()
                    .setColor('#ffffff')
                    .setTitle(`${player.name} finished studying!`)
                    .setDescription(reply)
            ]
        })

        player.currentAction = null
        player.currentActionData = null
        player.currentActionEndsAt = null
        player.currentActionDuration = null
        player.currentActionDescription = null
        SavingService.save()
    }
}

studyMagic.callbackGameLoopChannel = async function (location, channel) {

}

export default studyMagic