import UserCommand from "./UserCommand.js"
import GeneratorService from "../generator/GeneratorService.js"
import BaseGameService from "../generator/BaseGameService.js"
import {MessageEmbed} from "discord.js"
import SavingService from "../util/SavingService.js"
import WorldMapManager from "../worldmap/WorldMapManager.js"
import Enemy from "../entity/Enemy.js"
import Item from "../entity/Item.js"


/**
 *
 * @param {Enemy} enemy
 * @param {string} type
 * @return {Promise<Item>}
 */
async function generateEnemyItem(enemy, type) {
    const object = await GeneratorService.newWorkflow(
        BaseGameService.getGenerators(),
        BaseGameService.getWorkflows(),
        "loot",
        [
            {name: "name", value: enemy.name},
            {name: "difficulty", value: enemy.difficulty},
            {name: "type", value: type},
            {name: "rarity"},
            {name: "item"},
        ]
    )

    return new Item(object.rarity, object.type, object.item)
}

const spawnEnemy = new UserCommand(
    /^!spawn(Enemy)?/i,
    async (message, player, location, building) => {

        if (building) {
            await message.react("❌").catch(console.error)
            await message.reply(`${player.name} tried to attract an enemy, but ${player.firstname} is currently not outside.`)
            return true
        }

        message.react("✅").catch(console.error)

        const minTime = 10
        const maxTime = 30

        const timeUnavailableInSeconds = Math.floor(Math.random() * (maxTime - minTime) + minTime)
        player.currentAction = "attracting monsters"
        player.currentActionData = message
        player.currentActionDescription = null
        player.currentActionDuration = timeUnavailableInSeconds
        player.currentActionEndsAt = Date.now() + (1000 * timeUnavailableInSeconds)

        const embed = new MessageEmbed()
            .setColor('#6666ff')
            .setTitle(`${player.name} tries to attract a monster...`)
            .setDescription(`${player.firstname} begins to look around to find a monster. (ETA: ${minTime}-${maxTime}sec)`)

        await message.channel.send({embeds: [embed]})

        await message.delete().catch(console.error)

        SavingService.save()
    }
)

spawnEnemy.callbackGameLoop = async function (player, location, channel, building) {
    if (player?.currentAction === "attracting monsters") {
        if (Date.now() >= player.currentActionEndsAt) {

            const difficulty = WorldMapManager.getDifficulty(location)
            const enemiesTypes = WorldMapManager.getEnemiesTypes(location)

            const object = await GeneratorService.newWorkflow(
                BaseGameService.getGenerators(),
                BaseGameService.getWorkflows(),
                "spawnEnemy",
                [
                    {name: "difficulty", value: difficulty || "easiest"},
                    {
                        name: "type",
                        value: (enemiesTypes[Math.floor(Math.random() * enemiesTypes.length)]) || "slime"
                    },
                    {name: "name"},
                    {name: "encounterDescription"}
                ]
            )

            if (location.activeEnemy && location.activeEnemy.isAlive === false) {
                if (!location.enemyCorpses) location.enemyCorpses = []
                location.enemyCorpses.push(location.activeEnemy)
                location.activeEnemy = null
            }
            location.activeEnemy = new Enemy(object.name, object.difficulty, object.type, object.encounterDescription)

            let isAnimal = ['animal', 'animals'].includes(object?.type?.trim?.()?.toLowerCase() || '[undefined]')

            let weapon = isAnimal || Math.random() > 0.25 ? null : await generateEnemyItem(location.activeEnemy, "weapon")
            let armor = isAnimal || Math.random() > 0.25 ? null : await generateEnemyItem(location.activeEnemy, "armor")
            let accessory = isAnimal || Math.random() > 0.25 ? null : await generateEnemyItem(location.activeEnemy, "accessory")

            location.activeEnemy.weapon = weapon
            location.activeEnemy.armor = armor
            location.activeEnemy.accessory = accessory

            const embed = new MessageEmbed()
                .setColor('#6666ff')
                .setTitle(`An enemy appears!`)
                .setDescription(`${object?.encounterDescription || '[missing value]'}`)
                .addField("Name", object?.name || '[missing value]', true)
                .addField("Type", object?.type || '[missing value]', true)
                .addField("Difficulty", object?.difficulty || '[missing value]', true)

            if (weapon) {
                embed.addField("Weapon", WorldMapManager.getItemText(weapon), true)
            }
            if (armor) {
                embed.addField("Armor", WorldMapManager.getItemText(armor), true)
            }
            if (accessory) {
                embed.addField("Accessory", WorldMapManager.getItemText(accessory), true)
            }

            await player.currentActionData?.channel?.send({embeds: [embed]}).catch(console.error)

            // await channel.setTopic(`Current enemy: ${location.activeEnemy.name} (${location.activeEnemy.type} ${location.activeEnemy.difficulty})`).catch(console.error)

            player.currentAction = null
            player.currentActionData = null
            player.currentActionEndsAt = null
            player.currentActionDuration = null
            player.currentActionDescription = null
        }
    }

}

export default spawnEnemy