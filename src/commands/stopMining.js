import UserCommand from "./UserCommand.js"
import {MessageEmbed} from "discord.js"
import Colors from "../util/Colors.js"


const stopMining = new UserCommand(
    /^!stopMi?n?i?n?g?/i,
    async (message, player, location) => {

        if (player.currentAction !== 'mining') {
            await message.react("❌").catch(console.error)
            await message.reply(`You can't stop mining if you're not currently mining!`)
            return true
        }

        message.react("✅").catch(console.error)

        const miningTimeInMinutes = Math.floor(((Date.now() - (player.currentActionEndsAt - (player.currentActionDuration * 1000))) / 1000) / 60)

        player.currentAction = null
        player.currentActionData = null
        player.currentActionEndsAt = null
        player.currentActionDuration = null
        player.currentActionDescription = null

        const goldAmount = Math.floor(Math.random() * (1 + (miningTimeInMinutes * 2)))
        const taxes = location?.city?.mine?.tax ? Math.ceil(goldAmount * location.city.mine.tax) : 0

        player.gold += goldAmount - taxes

        if (location?.city?.mine) {
            location.city.mine.gold += taxes
        }

        await message.channel.send({
            embeds: [
                new MessageEmbed()
                    .setColor(Colors.GOLD)
                    .setTitle(`${player.name} stopped mining!`)
                    .setDescription(`${player.firstname} had enough of mining and decides to exit the mine after mining **${goldAmount} gold** in ${miningTimeInMinutes} minutes. (success: ${Math.round((goldAmount / (miningTimeInMinutes * 2)) * 200)}%)`)
                    .addField(`Mined gold`, `${goldAmount}`)
                    .addField(`Taxes`, `${taxes} gold (${(location?.city?.mine?.tax ? location.city.mine.tax * 100 : 0).toFixed(2)}%)`)
                    .addField(`Profit`, `${goldAmount - taxes} gold`)
                    .addField(`Total Character Gold`, `${player.gold}`)
            ]
        })

        await message.delete().catch(console.error)
    }
)

stopMining.actionBypass = true

export default stopMining