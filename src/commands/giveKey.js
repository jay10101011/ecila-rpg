import UserCommand from "./UserCommand.js"
import {MessageEmbed} from "discord.js"
import PlayerService from "../player/PlayerService.js"


const giveKey = new UserCommand(
    /^!give ?Key/i,
    async (message, player, location) => {

        const fullCommand = message.cleanContent.match(/^!give ?Key ?([0-9]*) ([^\n]*)/i)

        if (!fullCommand[1] || isNaN(parseInt(fullCommand[1])) || !fullCommand[2]) {
            await message.react("❌").catch(console.error)
            await message.reply(`Bad usage. Use like this: \`!give key 0 Freyja Ymir\``)
            return true
        }

        const otherPlayer = PlayerService.getPlayerByName(fullCommand[2])

        if (!otherPlayer || player.location !== otherPlayer.location) {
            await message.react("❌").catch(console.error)
            await message.reply(`Character ${fullCommand[2]} couldn't be found here!`)
            return true
        }

        const slot = parseInt(fullCommand[1])

        if (slot >= player.keys.length || slot < 0) {
            await message.react("❌").catch(console.error)
            await message.reply(`Cannot give key in slot [${slot}] (slot empty or not found)`)
            return true
        }

        if (!otherPlayer.keys) otherPlayer.keys = []
        otherPlayer.keys.push(player.keys[slot])

        otherPlayer.keys = [...new Set(otherPlayer.keys)]

        const embed = new MessageEmbed()
            .setColor('#665500')
            .setTitle(`${player.name} gave a key to ${otherPlayer.name}!`)
            .setDescription(`${player.firstname} gave the key to ${player.gender ? `her` : `his`} plot of land to ${otherPlayer.firstname}`)

        await message.channel.send({embeds: [embed]})

        await message.delete().catch(console.error)
    }
)

export default giveKey