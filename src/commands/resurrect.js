import UserCommand from "./UserCommand.js"
import {MessageEmbed} from "discord.js"
import PlayerHealth from "../player/PlayerHealth.js"
import SavingService from "../util/SavingService.js"
import DiscordService from "../util/DiscordService.js"


const resurrect = new UserCommand(
    /^!(sle?e?p?|resu?r?r?e?c?t?)/i,
    async (message, player, location) => {
        const timeUnavailableInSeconds = location.respawnTime

        player.currentAction = "resurrecting"
        player.currentActionDescription = `${player.name} is currently resurrecting`
        player.currentActionDuration = timeUnavailableInSeconds
        player.currentActionEndsAt = Date.now() + (1000 * timeUnavailableInSeconds)

        const embed = new MessageEmbed()
            .setColor('#ffffff')
            .setTitle(`${player.name} begins to resurrect...`)
            .setDescription(`${player.firstname} tried to get back to life. ${player.firstname} will not be able to do any action until ${player.gender ? 'she' : 'he'} resurrects. (${timeUnavailableInSeconds}sec left)`)

        await message.channel.send({embeds: [embed]})

        await message.delete().catch(console.error)
        SavingService.save()
    }
)

resurrect.callbackGameLoop = async function (player, location, channel) {
    if (player?.currentAction === "tired" && !player.isAlive) {
        player.currentAction = null
        player.currentActionData = null
        player.currentActionEndsAt = null
        player.currentActionDuration = null
        player.currentActionDescription = null
    }

    if (player?.currentAction === "resurrecting" || player?.currentAction === "sleeping") {
        if (Date.now() >= player.currentActionEndsAt) {
            const embed = new MessageEmbed()
                .setColor('#ffffff')
                .setTitle(`${player.name} is resurrected!`)
                .setDescription(`${player.firstname} wakes up completely healed! What a miracle!`)

            await channel.send({embeds: [embed]})

            const guild = DiscordService.bot.guilds.cache.find(g => g.id.toString() === process.env.DISCORD_SERVER_ID)
            const user = guild.members.cache.find(m => m.user.id === player.userId)

            if (user){
                await user.send({
                    embeds: [
                        new MessageEmbed()
                            .setColor('#ff0000')
                            .setTitle(`${player.name} resurrected!`)
                            .setDescription(`${player.firstname} resurrected in **Freedom and Consequences**!`)
                    ]
                })
            }

            player.health = new PlayerHealth()
            player.isAlive = true

            player.currentAction = null
            player.currentActionData = null
            player.currentActionEndsAt = null
            player.currentActionDuration = null
            player.currentActionDescription = null
            SavingService.save()
        }
    }
}

resurrect.actionBypass = true

export default resurrect