import createCharacter from "./createCharacter.js"
import spawnEnemy from "./spawnEnemy.js"
import take from "./take.js"
import attack from "./attack.js"
import equip from "./equip.js"
import unequip from "./unequip.js"
import drop from "./drop.js"
import look from "./look.js"
import inventory from "./inventory.js"
import sell from "./sell.js"
import resurrect from "./resurrect.js"
import whoIs from "./whoIs.js"
import eat from "./eat.js"
import travel from "./travel.js"
import butcher from "./butcher.js"
import studyMagic from "./studyMagic.js"
import focusSpell from "./focusSpell.js"
import castSpell from "./castSpell.js"
import mine from "./mine.js"
import enter from "./enter.js"
import exit from "./exit.js"
import buy from "./buy.js"
import giveKey from "./giveKey.js"
import store from "./store.js"
import stopMining from "./stopMining.js"
import retrieve from "./retrieve.js"
import createCity from "./createCity.js"

const commands = [
    createCity,
    stopMining,
    store,
    retrieve,
    giveKey,
    buy,
    enter,
    exit,
    castSpell,
    mine,
    focusSpell,
    studyMagic,
    butcher,
    travel,
    eat,
    createCharacter,
    spawnEnemy,
    take,
    attack,
    equip,
    unequip,
    drop,
    look,
    inventory,
    sell,
    resurrect,
    whoIs
]

export default commands