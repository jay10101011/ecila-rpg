import UserCommand from "./UserCommand.js"
import SavingService from "../util/SavingService.js"
import {MessageEmbed} from "discord.js"
import WorldMapManager from "../worldmap/WorldMapManager.js"


const unequip = new UserCommand(
    /^!un?eq?u?i?p?/i,
    async (message, player, location) => {

        const fullCommand = message.cleanContent.match(/^!un?eq?u?i?p? ?((ac)c?e?s?s?o?r?y?|(ar)m?o?r?|(w)e?a?p?o?n?)?/i)

        if (!(fullCommand?.[1])) {
            await message.react("❌").catch(console.error)
            await message.reply(`Bad usage. Use like this: \`!unequipWeapon\``)
            return true
        }

        if (player.inventory.length >= player.physicalLevel) {
            await message.react('🎒').catch(console.error)
            await message.react(`❌`).catch(console.error)
            await message.reply(`${player.name} tried to unequip an item, but ${player.gender ? 'she' : 'he'} is too encumbered.`)
            return true
        }


        let title = ``
        let description = ``
        if (fullCommand[1].toLowerCase().startsWith('w')) {
            if (player.weapon) {
                message.react("✅").catch(console.error)

                const oldWeapon = player.weapon

                player.inventory.push(oldWeapon)
                player.weapon = null

                title = `${player.name} unequips ${player.gender ? 'her' : 'his'} weapon.`
                description = `${player.firstname} removes ${player.gender ? 'her' : 'his'} weapon [${WorldMapManager.getItemText(oldWeapon)}] and places it in ${player.gender ? 'her' : 'his'} inventory slot [${player.inventory.length - 1}]`
            } else {
                await message.react(`❌`).catch(console.error)
                await message.reply(`${player.name} tried to unequip ${player.gender ? 'her' : 'his'} weapon, but have none.`)
                return true
            }
        } else if (fullCommand[1].toLowerCase().startsWith('ar')) {
            if (player.armor) {
                message.react("✅").catch(console.error)

                const oldWeapon = player.armor

                player.inventory.push(oldWeapon)
                player.armor = null

                title = `${player.name} unequips ${player.gender ? 'her' : 'his'} armor.`
                description = `${player.firstname} removes ${player.gender ? 'her' : 'his'} armor [${WorldMapManager.getItemText(oldWeapon)}] and places it in ${player.gender ? 'her' : 'his'} inventory slot [${player.inventory.length - 1}]`
            } else {
                await message.react(`❌`).catch(console.error)
                await message.reply(`${player.name} tried to unequip ${player.gender ? 'her' : 'his'} armor, but have none.`)
                return true
            }
        } else if (fullCommand[1].toLowerCase().startsWith('ac')) {
            if (player.accessory) {
                message.react("✅").catch(console.error)

                const oldWeapon = player.accessory

                player.inventory.push(oldWeapon)
                player.accessory = null

                title = `${player.name} unequips ${player.gender ? 'her' : 'his'} accessory.`
                description = `${player.firstname} removes ${player.gender ? 'her' : 'his'} accessory [${WorldMapManager.getItemText(oldWeapon)}] and places it in ${player.gender ? 'her' : 'his'} inventory slot [${player.inventory.length - 1}]`
            } else {
                await message.react(`❌`).catch(console.error)
                await message.reply(`${player.name} tried to unequip ${player.gender ? 'her' : 'his'} accessory, but have none.`)
                return true
            }
        }

        const embed = new MessageEmbed()
            .setColor('#665500')
            .setTitle(title)
            .setDescription(description)
            .addField('Equipped weapon', !player.weapon ? 'No Weapon' : `[${player.weapon.rarity} ${player.weapon.type}] ${player.weapon.name}`, true)
            .addField('Equipped armor', !player.armor ? 'No Armor' : `[${player.armor.rarity} ${player.armor.type}] ${player.armor.name}`, true)
            .addField('Equipped accessory', !player.accessory ? 'No accessory' : `[${player.accessory.rarity} ${player.accessory.type}] ${player.accessory.name}`, true)

        await message.channel.send({embeds: [embed]})

        await message.delete().catch(console.error)
        SavingService.save()

    }
)


export default unequip