{
  "name": "RPG Enemy Attack Generator",
  "description": "Generates RPG enemy attacks",
  "context": "[ ATTACK AGAINST A PLAYER: given player's and attacker's data, generates the attacker's attack description against the player. ]",
  "properties": [
    {
      "name": "player",
      "replaceBy": "Player's data:",
      "input": true
    },
    {
      "name": "enemy",
      "replaceBy": "Attacker's data:",
      "input": true
    },
    {
      "name": "description",
      "replaceBy": "Description of the attacker's attack:"
    }
  ],
  "placeholders": {
    "wounds": "wounds",
    "bloodLoss": "blood loss",
    "difficulty": "difficulty"
  },
  "aiParameters": {
    "temperature": 1,
    "repetition_penalty_slope": 9
  },
  "list": [
    {
      "player": "[ Player: Arshes Skree; gender: female; race: human; weapon: Iron Short Sword (rare weapon); armor: None; accessory: None; wounds: None; blood loss: None; status: healthy ]",
      "enemy": "[ Attacker: Orc; difficulty: easy; weapon: Small Knife (common weapon); armor: Leather Armor (common armor); accessory: None; wounds: None; blood loss: None; status: healthy ]",
      "description": "The orc attacks Arshes Skree using his small knife. Arshes manages to easily dodge the attack, but she still gets tired from the efforts."
    },
    {
      "player": "[ Player: Freyja Ymir; gender: female; race: human; weapon: None; armor: None; accessory: None; wounds: bruised left arm, eviscerated; blood loss: major; status: badly injured ]",
      "enemy": "[ Attacker: Troll; difficulty: hard; weapon: Large Wooden Club (uncommon weapon); armor: None; accessory: None; wounds: cut (right arm); blood loss: minor; status: slightly wounded ]",
      "description": "The troll attacks Freyja Ymir with his large wooden club, and hits Freyja right in the face. Freyja is severely injured, and she falls down before dying shortly after."
    },
    {
      "player": "[ Player: Thaddeus Morgan; gender: male; race: human; weapon: None; armor: Divine Protection (legendary armor); accessory: None; wounds: None; blood loss: None; status: healthy ]",
      "enemy": "[ Attacker: Minotaur; difficulty: normal; weapon: Spear (rare weapon); armor: None; accessory: None; wounds: None; blood loss: None; status: healthy ]",
      "description": "The minotaur uses a spear to attack Thaddeus Morgan, but Thaddeus' armor deflects the blow! Thaddeus is completely unscathed."
    },
    {
      "player": "[ Player: Magnus Shredmur; gender: male; race: human; weapon: None; armor: None; accessory: None; wounds: None; blood loss: None; status: healthy ]",
      "enemy": "[ Attacker: Skeleton; difficulty: arduous; weapon: Scimitar (rare weapon); armor: Chainmail (rare armor); accessory: None; wounds: None; blood loss: None; status: undead ]",
      "description": "The skeleton brandishes his scimitar and violently hits Magnus Shredmur across his torso. Magnus' blood sprays everywhere and his guts fall out of his body as he feels his life force escaping."
    },
    {
      "player": "[ Player: Asherah Balek; gender: female; race: human; weapon: Bow (rare weapon); armor: None; accessory: None; wounds: None; blood loss: None; status: healthy ]",
      "enemy": "[ Attacker: Dire Wolf; difficulty: easy; weapon: Dire Wolf Fangs (uncommon weapon); armor: Dire Wolf Leather (uncommon armor); accessory: None; wounds: None; blood loss: None; status: healthy ]",
      "description": "The dire wolf jumps at Asherah Balek and gives a powerful bite to her right leg. Asherah is badly injured by the beast's fangs, and she bleeds heavily."
    }
  ]
}