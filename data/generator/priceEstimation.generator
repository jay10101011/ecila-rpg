{
  "name": "RPG Item Price Generator",
  "description": "Estimates an item price",
  "context": "[ ITEM PRICE ESTIMATION: given an item name, type and rarity, gives a price estimation for that item. ]",
  "placeholders": {
    "currency": "gold"
  },
  "properties": [
    {
      "name": "item",
      "replaceBy": "Item Name:",
      "input": true
    },
    {
      "name": "type",
      "replaceBy": "Item Type:",
      "input": true
    },
    {
      "name": "rarity",
      "replaceBy": "Item Rarity:",
      "input": true
    },
    {
      "name": "price",
      "replaceBy": "Item Price:"
    }
  ],
  "list": [
    {
      "item": "Iron Broad Sword",
      "type": "weapon",
      "rarity": "common",
      "price": "100 ${currency}"
    },
    {
      "item": "Unicorn Horn",
      "type": "component",
      "rarity": "uncommon",
      "price": "300 ${currency}"
    },
    {
      "item": "Rare Dragonscale Armor",
      "type": "armor",
      "rarity": "rare",
      "price": "1250 ${currency}"
    },
    {
      "item": "Rabbit Pelt",
      "type": "component",
      "rarity": "very common",
      "price": "8 ${currency}"
    },
    {
      "item": "Old Wooden Shield",
      "type": "shield",
      "rarity": "very common",
      "price": "30 ${currency}"
    },
    {
      "item": "Giant Centipede's Carapace",
      "type": "component",
      "rarity": "uncommon",
      "price": "70 ${currency}"
    },
    {
      "item": "Dagger of Death",
      "type": "weapon",
      "rarity": "epic",
      "price": "7500 ${currency}"
    },
    {
      "item": "Glowing Staff of Fireballs",
      "type": "magic item",
      "rarity": "epic",
      "price": "3000 ${currency}"
    },
    {
      "item": "Silver Crossbow",
      "type": "weapon",
      "rarity": "rare",
      "price": "750 ${currency}"
    },
    {
      "item": "Mithril Plate Mail",
      "type": "armor",
      "rarity": "rare",
      "price": "800 ${currency}"
    },
    {
      "item": "Black Robes of the Necromancy School",
      "type": "equipment",
      "rarity": "epic",
      "price": "5000 ${currency}"
    },
    {
      "item": "Leather Breastplate of Disguise",
      "type": "equipment",
      "rarity": "rare",
      "price": "650 ${currency}"
    },
    {
      "item": "Mighty Sword of Power",
      "type": "weapon",
      "rarity": "legendary",
      "price": "35000 ${currency}"
    }
  ]
}