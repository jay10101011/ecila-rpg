{
  "name": "RPG Attack Generator",
  "description": "Generates RPG attacks given a weapon",
  "aiParameters": {
    "temperature": 1,
    "repetition_penalty": 1.02
  },
  "context": "[ HEALTH STATUS CALCULATION: given the target's current wounds, blood loss level, health status and the description of the player's attack, calculates the target's new health status. ]\n[ Note: only calculates target's health status, NOT player's health status. ]\n[ Fatal wounds: if the target received fatal wounds, its health status always becomes \"dead\". ]",
  "properties": [
    {
      "name": "enemyCurrentWounds",
      "replaceBy": "Target's current wounds:",
      "input": true
    },
    {
      "name": "enemyCurrentBloodLoss",
      "replaceBy": "Target's current blood loss level:",
      "input": true
    },
    {
      "name": "enemyCurrentStatus",
      "replaceBy": "Target's current health status:",
      "input": true
    },
    {
      "name": "description",
      "replaceBy": "Description of player attack:",
      "input": true
    },
    {
      "name": "wounds",
      "replaceBy": "Target's newly added wound(s):",
      "input": true
    },
    {
      "name": "status",
      "replaceBy": "Target's new health status:"
    }
  ],
  "list": [
    {
      "enemyCurrentWounds": "none",
      "enemyCurrentBloodLoss": "none",
      "enemyCurrentStatus": "healthy",
      "description": "Arshes Skree attacks the orc using his bare hands. The orc manages to dodge or block some of the attacks, but still gets bruised and tired from the efforts.",
      "wounds": "bruised torso, general tiredness",
      "status": "slightly wounded"
    },
    {
      "enemyCurrentWounds": "none",
      "enemyCurrentBloodLoss": "none",
      "enemyCurrentStatus": "healthy",
      "description": "Arshes Skree attacks the orc using his fists. It's not very effective, but Arshes manages to break the orc's nose.",
      "wounds": "broken nose",
      "status": "slightly wounded"
    },
    {
      "enemyCurrentWounds": "broken nose",
      "enemyCurrentBloodLoss": "minor",
      "enemyCurrentStatus": "slightly wounded",
      "description": "Amenhotep Akhentaneb strikes the orc in the stomach with his iron sword. Amenhotep cuts into orc's abdomen and some guts come out, spreading his blood everywhere.",
      "wounds": "eviscerated abdomen",
      "status": "heavily wounded"
    },
    {
      "enemyCurrentWounds": "broken nose, broken ribs, eviscerated",
      "enemyCurrentBloodLoss": "major",
      "enemyCurrentStatus": "heavily wounded",
      "description": "Erya Kuroi punches the semi-conscious orc in the torso. Her blows don't do much, but the orc is already so badly mangled that it succumbs to his severe wounds and dies.",
      "wounds": "bruised torso, fatal wounds",
      "status": "dead"
    },
    {
      "enemyCurrentWounds": "broken nose, broken ribs",
      "enemyCurrentBloodLoss": "minimal",
      "enemyCurrentStatus": "heavily wounded",
      "description": "Reezea Storar swings her iron longsword at the orc's throat. Due to his wounds, he fails to dodge and Reezea cuts his head off.",
      "wounds": "decapitated",
      "status": "dead"
    },
    {
      "enemyCurrentWounds": "none",
      "enemyCurrentBloodLoss": "none",
      "enemyCurrentStatus": "healthy",
      "description": "Erya Kuroi attacks the goblin using her bare fists. The goblin is an agile fighter, and dodges most of her attacks.",
      "wounds": "small bruises",
      "status": "very slightly wounded"
    },
    {
      "enemyCurrentWounds": "none",
      "enemyCurrentBloodLoss": "none",
      "enemyCurrentStatus": "healthy",
      "description": "Reezea Storar slashes the goblin across the chest with her knife. The goblin's cloth armor does nothing for protection, and it slowly starts to bleed from the cut.",
      "wounds": "slashed chest",
      "status": "slightly wounded"
    },
    {
      "enemyCurrentWounds": "slashed chest",
      "enemyCurrentBloodLoss": "moderate",
      "enemyCurrentStatus": "moderately wounded",
      "description": "Yakhil Tern pierces the goblin with her wooden spear. Akhil's spear flies into the goblin's stomach and guts, spreading his blood and intestines everywhere.",
      "wounds": "disemboweled",
      "status": "dead"
    },
    {
      "enemyCurrentWounds": "none",
      "enemyCurrentBloodLoss": "none",
      "enemyCurrentStatus": "healthy",
      "description": "Akhil Tern swings her steel axe at the boar with strength, severing its front leg.",
      "wounds": "amputated right leg",
      "status": "heavily wounded"
    },
    {
      "enemyCurrentWounds": "none",
      "enemyCurrentBloodLoss": "none",
      "enemyCurrentStatus": "healthy",
      "description": "Kalum Derant swiftly attacks the kobold with his copper dagger, cutting kobold's left ear right off.",
      "wounds": "severed left ear",
      "status": "moderately wounded"
    },
    {
      "enemyCurrentWounds": "lacerations (right arm), severed limb (left arm)",
      "enemyCurrentBloodLoss": "major",
      "enemyCurrentStatus": "heavily wounded",
      "description": "Kalum Derant stabs the enemy in the chest, pushing the blade deep through the kobold's heart. The kobold is badly wounded and doesn't even fight back.",
      "wounds": "pierced heart",
      "status": "dead"
    },
    {
      "enemyCurrentWounds": "disemboweled, amputated (right leg)",
      "enemyCurrentBloodLoss": "major",
      "enemyCurrentStatus": "heavily wounded",
      "description": "Tasha Kanata strikes the troll with her wooden club, only doing light bruises on its sternum. The troll is however badly in shape and bleeding profusely, and just crumbles to the floor few seconds later, dead.",
      "wounds": "lightly bruised sternum, fatal wounds",
      "status": "dead"
    }
  ]
}