{
  "name": "RPG Attack Generator",
  "description": "Generates RPG attacks given a weapon",
  "aiParameters": {
    "temperature": 1,
    "repetition_penalty": 1.02
  },
  "context": "[ HEALTH STATUS CALCULATION: given the player's current wounds, blood loss level, health status and the description of the attacker's attack, calculates the player's new health status. ]\n[ Note: only calculates player's health status, NOT attacker's health status. ]\n[ Fatal wounds: if the player received fatal wounds, his health status always becomes \"dead\". ]",
  "properties": [
    {
      "name": "enemyCurrentWounds",
      "replaceBy": "Player's current wounds:",
      "input": true
    },
    {
      "name": "enemyCurrentBloodLoss",
      "replaceBy": "Player's current blood loss level:",
      "input": true
    },
    {
      "name": "enemyCurrentStatus",
      "replaceBy": "Player's current health status:",
      "input": true
    },
    {
      "name": "description",
      "replaceBy": "Description of attacker's attack:",
      "input": true
    },
    {
      "name": "wounds",
      "replaceBy": "Player's newly added wound(s):",
      "input": true
    },
    {
      "name": "status",
      "replaceBy": "Player's new health status:"
    }
  ],
  "list": [
    {
      "enemyCurrentWounds": "none",
      "enemyCurrentBloodLoss": "none",
      "enemyCurrentStatus": "healthy",
      "description": "The orc attacks Arshes Skree using his bare hands. Arshes manages to dodge or block some of the attacks, but still gets bruised and tired from the efforts.",
      "wounds": "bruises, tired",
      "status": "slightly wounded"
    },
    {
      "enemyCurrentWounds": "none",
      "enemyCurrentBloodLoss": "none",
      "enemyCurrentStatus": "healthy",
      "description": "The troll attacks Freyja Ymir with his large wooden club, and hits Freyja right in the face. Freyja is severely injured, and she falls down before dying shortly after.",
      "wounds": "fractured skull, fatal wounds",
      "status": "dead"
    },
    {
      "enemyCurrentWounds": "broken nose",
      "enemyCurrentBloodLoss": "minor",
      "enemyCurrentStatus": "slightly wounded",
      "description": "The minotaur uses a spear to attack Thaddeus Morgan, but Thaddeus' armor deflects the blow! Thaddeus is completely unscathed.",
      "wounds": "none",
      "status": "slightly wounded"
    },
    {
      "enemyCurrentWounds": "broken nose, broken ribs, eviscerated",
      "enemyCurrentBloodLoss": "major",
      "enemyCurrentStatus": "heavily wounded",
      "description": "The skeleton brandishes his scimitar and violently hits Magnus Shredmur across his torso. Magnus' blood sprays everywhere and his guts fall out of his body as he feels his life force escaping.",
      "wounds": "eviscerated",
      "status": "dying"
    },
    {
      "enemyCurrentWounds": "none",
      "enemyCurrentBloodLoss": "none",
      "enemyCurrentStatus": "healthy",
      "description": "The dire wolf jumps at Asherah Balek and gives a powerful bite to her right leg. Asherah is badly injured by the beast's fangs, and she bleeds heavily.",
      "wounds": "mangled right leg",
      "status": "wounded"
    }
  ]
}