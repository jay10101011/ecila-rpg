{
  "name": "RPG Item Generator",
  "description": "Generates an RPG item given an enemy and its difficulty",
  "context": "[ ITEM LOOT GENERATOR: given a defeated enemy name and difficulty, generates an item loot. ]",
  "placeholders": {
    "difficulty": "difficulty"
  },
  "aiParameters": {
    "temperature": 1.5,
    "repetition_penalty": 1.02
  },
  "properties": [
    {
      "name": "name",
      "replaceBy": "Defeated Enemy Name:",
      "input": true
    },
    {
      "name": "difficulty",
      "replaceBy": "Defeated Enemy Difficulty:",
      "input": true
    },
    {
      "name": "type",
      "replaceBy": "Looted Item Type:"
    },
    {
      "name": "rarity",
      "replaceBy": "Looted Item Rarity:"
    },
    {
      "name": "item",
      "replaceBy": "Looted Item Name:"
    }
  ],
  "list": [
    {
      "name": "Orc Soldier",
      "difficulty": "medium",
      "item": "Simple Iron Broad Sword",
      "type": "weapon",
      "rarity": "common"
    },
    {
      "name": "Rabid Unicorn",
      "difficulty": "hard",
      "item": "Magical Unicorn Horn",
      "type": "component",
      "rarity": "uncommon"
    },
    {
      "name": "Rare Golden Dragon",
      "difficulty": "arduous",
      "item": "Rare Dragonscale Armor",
      "type": "armor",
      "rarity": "rare"
    },
    {
      "name": "Enraged Rabbit",
      "difficulty": "very easy",
      "item": "Soft Rabbit Pelt",
      "type": "component",
      "rarity": "very common"
    },
    {
      "name": "Feral Deer",
      "difficulty": "medium",
      "item": "Fine Deer Meat",
      "type": "meat",
      "rarity": "uncommon"
    },
    {
      "name": "Famished Goblin",
      "difficulty": "easy",
      "item": "Old Wooden Shield",
      "type": "shield",
      "rarity": "very common"
    },
    {
      "name": "Giant Centipede",
      "difficulty": "difficult",
      "item": "Giant Centipede's Carapace",
      "type": "component",
      "rarity": "uncommon"
    },
    {
      "name": "Skeleton Knight",
      "difficulty": "extremely difficult",
      "item": "Sharp Dagger of Death",
      "type": "weapon",
      "rarity": "epic"
    },
    {
      "name": "Dark Wizard",
      "difficulty": "impossible",
      "item": "Glowing Staff of Fireballs",
      "type": "magic item",
      "rarity": "epic"
    },
    {
      "name": "Dire Wolf",
      "difficulty": "challenging",
      "item": "Piercing Silver Crossbow",
      "type": "weapon",
      "rarity": "rare"
    },
    {
      "name": "Molten Lizard",
      "difficulty": "hard",
      "item": "Mithril Plate Mail",
      "type": "armor",
      "rarity": "rare"
    },
    {
      "name": "Undead Archmage",
      "difficulty": "extremely difficult",
      "item": "Black Robes of the Necromancy School",
      "type": "equipment",
      "rarity": "epic"
    },
    {
      "name": "Chameleon Ogre",
      "difficulty": "medium",
      "item": "Leather Breastplate of Disguise",
      "type": "equipment",
      "rarity": "rare"
    },
    {
      "name": "Evil Overlord",
      "difficulty": "extreme",
      "item": "Evil Staff of Doom",
      "type": "magic weapon",
      "rarity": "legendary"
    },
    {
      "name": "Golden Dragon",
      "difficulty": "legendary",
      "item": "Golden Axe of Ultimate Power",
      "type": "mythic enchanted weapon",
      "rarity": "legendary"
    }
  ]
}