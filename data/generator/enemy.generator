{
  "name": "RPG Enemy Generator",
  "description": "Generates an RPG enemy given a difficulty",
  "context": "[ ENEMY ENCOUNTER GENERATOR: given a difficulty, generates one enemy and its encounter description. ]",
  "placeholders": {
    "difficulty": "difficulty"
  },
  "properties": [
    {
      "name": "difficulty",
      "replaceBy": "Enemy Difficulty:",
      "input": true
    },
    {
      "name": "type",
      "replaceBy": "Enemy Type:",
      "input": true
    },
    {
      "name": "name",
      "replaceBy": "Enemy Name:"
    },
    {
      "name": "encounterDescription",
      "replaceBy": "Encounter Description:"
    }
  ],
  "list": [
    {
      "name": "Menacing Orc Soldier",
      "difficulty": "medium",
      "type": "orc",
      "encounterDescription": "The menacing orc soldier with it's worn leather armor and broad sword slowly and silently approach you from deep in the woods reading its weapon for a powerful and swift attack."
    },
    {
      "name": "Rare Golden Dragon",
      "difficulty": "arduous",
      "type": "dragon",
      "encounterDescription": "The rare majestic golden dragon circles far above the trees looking for its next easy meal, when it spots you and decides that you are it."
    },
    {
      "name": "Famished Goblin Scout",
      "difficulty": "easy",
      "type": "goblin",
      "encounterDescription": "Deep in a dark and damp cave a emaciated goblin scout spots your torch light and decides that it has found its first full meal in weeks."
    },
    {
      "name": "Tired Kobold Miner",
      "difficulty": "very easy",
      "type": "kobold",
      "encounterDescription": "A lone kobold miner works tirelessly under a bright sun mining precious minerals. Although he doesn't look up often, you know that if you were to get too close he would easily take care of you."
    },
    {
      "name": "Giant Black Centipede",
      "difficulty": "difficult",
      "type": "insect",
      "encounterDescription": "While traveling through the redwood forest a giant black centipede leaps off an overhead branch aming for your face with its venomous mouth."
    },
    {
      "name": "Skeleton Knight",
      "difficulty": "extremely difficult",
      "type": "skeleton",
      "encounterDescription": "As you travel through the dreary musty crypt you hear the rattling sounds of bones and steel as a skeleton knight approaches you from the dark."
    },
    {
      "name": "Dark Wizard",
      "difficulty": "impossible",
      "type": "wizard",
      "encounterDescription": "The dark wizard watches over the plains surrounding his mage tower. He spots you and prepares a warm welcome for you with his most powerful fire ball spell."
    },
    {
      "name": "Large Dire Wolf",
      "difficulty": "challenging",
      "type": "animal",
      "encounterDescription": "You hear the crunching of bones and flesh as you approach a small cave hidden in the rock face of the mountain. You can smell and hear the large dire wolf enjoying its last kill well before you can see it."
    },
    {
      "name": "Undead Archmage",
      "difficulty": "extremely difficult",
      "type": "undead",
      "encounterDescription": "You stumble upon the ruins of what looks like some ancient castle on the outskirts of town. The undead archmage stares at you with its glowing yellow eyes before it begins to chant and wave his hands around in mystic circles."
    },
    {
      "name": "Small Chameleon Ogre",
      "difficulty": "medium",
      "type": "orgre",
      "encounterDescription": "You have heard stories about small ogres that turn into other creatures but never thought they would come true. The chameleon ogre is no different, changing colors to blend in with it's surroundings in order to ambush you."
    },
    {
      "name": "Vicious Vampire",
      "difficulty": "medium",
      "type": "vampire",
      "encounterDescription": "You feel the cold chill of death as you enter a room in the abandoned manor house where you find the vampire feasting on the remains of a young woman."
    },
    {
      "name": "Rogue Fire Elemental",
      "difficulty": "difficult",
      "type": "elemental",
      "encounterDescription": "You see a rogue fire elemental begins to appear in front of you. It starts off as a simple fireball, but as it gets bigger it becomes more dangerous, taking on new shapes and forms."
    }
  ],
  "aiParameters": {
    "temperature": 2,
    "bad_words_ids": [
      [
        40,
        24595,
        968
      ],
      [
        443,
        24595,
        968
      ],
      [
        40,
        24595,
        968
      ],
      [
        443,
        24595,
        968
      ],
      [
        10993,
        5993,
        11965
      ],
      [
        11488,
        5993,
        11965
      ],
      [
        72,
        24595,
        968
      ],
      [
        564,
        1559,
        968
      ],
      [
        40,
        24595,
        968
      ],
      [
        443,
        24595,
        968
      ],
      [
        44,
        706,
        3502
      ],
      [
        43120,
        3502
      ],
      [
        44,
        706,
        3502
      ],
      [
        43120,
        3502
      ],
      [
        44,
        9324,
        3271,
        5942
      ],
      [
        611,
        9324,
        3271,
        5942
      ],
      [
        76,
        706,
        3502
      ],
      [
        465,
        706,
        3502
      ],
      [
        44,
        706,
        3502
      ],
      [
        43120,
        3502
      ],
      [
        56,
        14503
      ],
      [
        411,
        14503
      ],
      [
        56,
        14503
      ],
      [
        411,
        14503
      ],
      [
        56,
        3271,
        55,
        1410
      ],
      [
        411,
        3271,
        55,
        1410
      ],
      [
        88,
        14503
      ],
      [
        36162
      ],
      [
        56,
        14503
      ],
      [
        411,
        14503
      ],
      [
        35,
        6364
      ],
      [
        34105
      ],
      [
        35,
        6364
      ],
      [
        34105
      ],
      [
        8998,
        15286
      ],
      [
        8728,
        15286
      ],
      [
        67,
        6364
      ],
      [
        17267
      ],
      [
        35,
        6364
      ],
      [
        34105
      ],
      [
        51,
        8902,
        953
      ],
      [
        416,
        8902,
        953
      ],
      [
        51,
        8902,
        953
      ],
      [
        416,
        8902,
        953
      ],
      [
        51,
        2925,
        39076
      ],
      [
        416,
        2925,
        39076
      ],
      [
        83,
        8902,
        953
      ],
      [
        29948
      ],
      [
        51,
        8902,
        953
      ],
      [
        416,
        8902,
        953
      ]
    ]
  }
}