{
  "name": "Wound Detection Generator",
  "description": "Generates wounds given a description of an attack",
  "aiParameters": {
    "temperature": 1,
    "repetition_penalty": 1.02
  },
  "context": "[ WOUNDS DETECTION: given a description of a player's attack against a target, detects the new target's wound(s). ]\n[ Note: only detects target's wounds, NOT player's wounds. ]\n[ Fatal wounds: if the target dies, a wound named \"fatal wound\" is added. ]",
  "properties": [
    {
      "name": "description",
      "replaceBy": "Description of the player's attack:",
      "input": true
    },
    {
      "name": "wounds",
      "replaceBy": "Target's new wound(s):"
    }
  ],
  "list": [
    {
      "description": "Arshes Skree attacks the orc using his bare hands. The orc manages to dodge or block some of the attacks, but still gets bruised and tired from the efforts.",
      "wounds": "bruised torso, general tiredness"
    },
    {
      "description": "Arshes Skree attacks the orc using his fists. It's not very effective, but Arshes manages to break the orc's nose.",
      "wounds": "broken nose"
    },
    {
      "description": "Amenhotep Akhentaneb strikes the orc in the stomach with his iron sword. Amenhotep cuts into orc's abdomen and some guts come out, spreading his blood everywhere.",
      "wounds": "eviscerated abdomen"
    },
    {
      "description": "Erya Kuroi punches the semi-conscious orc in the torso. Her blows don't do much, but the orc is already so badly mangled that it succumbs to his severe wounds and dies.",
      "wounds": "fatal wounds"
    },
    {
      "description": "Reezea Storar swings her iron longsword at the orc's throat. Due to his wounds, he fails to dodge and Reezea cuts his head off.",
      "wounds": "decapitated, fatal wound"
    },
    {
      "description": "Erya Kuroi attacks the goblin using her bare fists. The goblin is an agile fighter, and dodges most of her attacks.",
      "wounds": "none"
    },
    {
      "description": "Reezea Storar slashes the goblin across the chest with her knife. The goblin's cloth armor does nothing for protection, and it slowly starts to bleed from the cut.",
      "wounds": "slashed chest"
    },
    {
      "description": "Yakhil Tern pierces the goblin with her wooden spear. Akhil's spear flies into the goblin's stomach and guts, spreading his blood and intestines everywhere.",
      "wounds": "disemboweled stomach"
    },
    {
      "description": "Akhil Tern swings her steel axe at the boar with strength, severing its front leg.",
      "wounds": "severed front leg"
    },
    {
      "description": "Kalum Derant swiftly attacks the kobold with his copper dagger, cutting kobold's left ear right off.",
      "wounds": "severed left ear"
    },
    {
      "description": "Kalum Derant stabs the enemy in the chest, pushing the blade deep through the kobold's heart. The kobold is badly wounded and doesn't even fight back.",
      "wounds": "pierced heart"
    },
    {
      "description": "Tasha Kanata strikes the troll with her wooden club, only doing light bruises on its sternum. The troll is however badly in shape and bleeding profusely, and just crumbles to the floor few seconds later, dead.",
      "wounds": "bruised chest, fatal wounds"
    },
    {
      "description": "Balok Tunks casts the Make Drunk spell on the ogre, infusing his blood with alcohol. The ogre barely notices the effects.",
      "wounds": "general drunkenness"
    },
    {
      "description": "Lagana Tumana fires a poisonous arrow from her elven bow at the giant eagle. The giant eagle is hit straight through its left wing but keeps flying.",
      "wounds": "punctured left wing, poisoned"
    }
  ]
}