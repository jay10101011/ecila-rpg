# How to contribute

TL;DR:
- Suggest new features or changes to the existing ones
- Help us write good generators

# Generators

Generators are a notion inherited from AIDungeon's "evalbots"  
They are prompts that are executed before or after the actual AI story generation, allowing to do useful things to
either the input or the output  
Fundamentally, generators here are a tool to build a prompt using several examples

## What are they for?

Generators can do a lot of useful things, from format conversion to data extraction, or plain item generation  
Below is a non-exhaustive list of things they can do:

- Generate new things given examples (enemies, items, spells, etc)
- Extract data (detect wounds)
- Transform data (apply wounds to change health status)
- Predict data (price estimation)

# Generator Format

Each generator file format is a JSON file containing the necessary data to build a prompt

Minimal format is as follows:

```
{
  "name": "Enemy Difficulty Detector",
  "description": "Detects the enemy difficulty given an enemy name",
  "context": "[ Outputs the enemy difficulty given the enemy name as input ]",
  "properties": [
    {
      "name": "enemyName",
      "replaceBy": "Input Enemy Name:",
      "input": true
    },
    {
      "name": "enemyDifficulty",
      "replaceBy": "Output Enemy Difficulty:"
    }
  ],
  "aiParameters": {
    "repetition_penalty": 1.01
  }
  "list": [
    {
      "enemyName": "Orc Warlock",
      "enemyDifficulty": "hard"
    },
    {
      "enemyName": "Rabid Mouse",
      "enemyDifficulty": "easy"
    }
  ]
}
```

Let's review each property one by one to explain them

## `name`

The name of your generator  
For display purpose only

## `description`

The description of your generator  
For display purpose only

## `context`

Context is a text that will be inserted at the very top of the generated prompt  
It is used to give the AI some context about what you're trying to do in this generator  
It's a very powerful tool if used correctly, but the wording needs to be chosen with care or it might have negative
impacts  
You can set this value to `null` or empty string `""` to disable context injection

## `properties`

Defines which properties will be used in the examples, and in which order  
It should be provided as a list of `{"name": "propertyName", "replaceBy": "replacement value"}` like in the example  
`name` is the property name, and `replaceBy` is the property name replacement as it will appear in the generated prompt

As for everything that ends up in the prompt, words used in the `replaceBy` value should be carefully picked and tested  
Using words such as `input` and `output` in the name replacement `replaceBy` seems to help in some cases

***Important: the order in which properties are defined will be the order used in the generated prompt!***

## `aiParameters`

You can use those to override the default AI generation parameters used by each generator!  
Most of the time, you'll only use the `repetition_penalty` and `temperature` values, but you can also use it to add banned words or phrase biases!

## `exampleList`

The main part of a generator, it's a list of examples of things you want to generate  
It should be provided as a list of flat JSON objects, in which each property is one of the defined ones in `properties`
above

You can put as many examples as you want here, the more the better!  
The generator will first shuffle the examples, then put as many as it can into the prompt

***Note: don't put stuff that you really want the AI to generate as examples!***
It seems counterintuitive, but you should only write examples of things ***like*** the thing you're trying to generate  
If you put **the** things you want the AI to generate as example, they will not come in the results as the AI tries to
generate new samples, not reproduce them